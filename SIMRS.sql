-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 22, 2020 at 05:18 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `SIMRS`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `photo`, `description`) VALUES
(9, '1600252596_22af3c2376cfd913a661.png', 'woyyy');

-- --------------------------------------------------------

--
-- Table structure for table `administrasi`
--

CREATE TABLE `administrasi` (
  `id` int(11) NOT NULL,
  `nama_pasien` varchar(100) DEFAULT NULL,
  `tanggal_lahir` varchar(30) DEFAULT NULL,
  `no_rekam_medis` int(11) DEFAULT NULL,
  `jenis_kelamin` varchar(15) DEFAULT NULL,
  `berat_badan` int(11) DEFAULT NULL,
  `no_telepon_pasien` varchar(20) DEFAULT NULL,
  `alamat_pasien` varchar(250) DEFAULT NULL,
  `nama_dokter` varchar(50) DEFAULT NULL,
  `sip_dokter` varchar(50) DEFAULT NULL,
  `paraf_dokter` varchar(50) DEFAULT NULL,
  `tanggal_resep` varchar(50) DEFAULT NULL,
  `photo_resep` varchar(100) DEFAULT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `creator` varchar(50) DEFAULT NULL,
  `updated_at` varchar(50) DEFAULT NULL,
  `jenis_penjamin` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `administrasi`
--

INSERT INTO `administrasi` (`id`, `nama_pasien`, `tanggal_lahir`, `no_rekam_medis`, `jenis_kelamin`, `berat_badan`, `no_telepon_pasien`, `alamat_pasien`, `nama_dokter`, `sip_dokter`, `paraf_dokter`, `tanggal_resep`, `photo_resep`, `created_at`, `creator`, `updated_at`, `jenis_penjamin`) VALUES
(1, 'Wowo Wijaya', '2020-09-18', 235, 'laki-laki', 55, '081517449177', 'Jati Asih asih', 'dr wiliam', 'SIP', 'paraf', '2020-09-11', '', '14-Sep-2020 10:30:41', 'adehafidudin@gmail.com', '17-Sep-2020 11:30:27', 'BPJS'),
(3, 'Yudhi', '2000-09-07', 22, 'laki-laki', 57, '081517449898', 'Jalan Pinang', 'dr. Ruly', '', 'Paraf', '2020-09-10', '', NULL, 'adehafidudin@gmail.com', '17-Sep-2020 09:30:12', 'Umum'),
(28, 'Rudi Habibi', '1998-07-16', 356, 'laki-laki', 65, '081517449176', 'Jakarta Raya', 'Dr. Wiliam', '', '', '2020-09-04', '', NULL, 'adehafidudin@gmail.com', '17-Sep-2020 09:30:26', 'Asuransi');

-- --------------------------------------------------------

--
-- Table structure for table `farmasetis`
--

CREATE TABLE `farmasetis` (
  `id` int(11) NOT NULL,
  `nama_obat` varchar(250) DEFAULT NULL,
  `bentuk_sediaan` varchar(30) DEFAULT NULL,
  `kekuatan_sediaan` varchar(250) DEFAULT NULL,
  `satuan_sediaan` varchar(15) DEFAULT NULL,
  `jumlah_obat` int(11) DEFAULT NULL,
  `aturan_pakai` varchar(50) DEFAULT NULL,
  `cara_pakai` varchar(250) DEFAULT NULL,
  `stabilitas_obat` varchar(50) DEFAULT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `creator` varchar(50) DEFAULT NULL,
  `updated_at` varchar(50) DEFAULT NULL,
  `indikasi_obat` varchar(255) DEFAULT NULL,
  `dosis_obat` varchar(100) DEFAULT NULL,
  `rute_pemberian_obat` varchar(250) DEFAULT NULL,
  `tepat_waktu` varchar(250) DEFAULT NULL,
  `duplikasi` varchar(250) DEFAULT NULL,
  `alergi` varchar(250) DEFAULT NULL,
  `interaksi_obat` varchar(50) DEFAULT NULL,
  `kontraindikasi_obat` varchar(100) DEFAULT NULL,
  `efek_samping` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `farmasetis`
--

INSERT INTO `farmasetis` (`id`, `nama_obat`, `bentuk_sediaan`, `kekuatan_sediaan`, `satuan_sediaan`, `jumlah_obat`, `aturan_pakai`, `cara_pakai`, `stabilitas_obat`, `created_at`, `creator`, `updated_at`, `indikasi_obat`, `dosis_obat`, `rute_pemberian_obat`, `tepat_waktu`, `duplikasi`, `alergi`, `interaksi_obat`, `kontraindikasi_obat`, `efek_samping`) VALUES
(24, 'Mixagrip', 'Pilulae (PIL)', 'kekuatan sediaan', 'satuan sediaan', 1, '3 x 1', 'Diminum', 'stabilitas obat', NULL, 'adehafidudin@gmail.com', '21-Sep-2020 07:30:17', 'indikasi obat', 'dosis obat', 'rute', 'tepat waktu', 'duplikasi', 'alergi', 'interaksi obat', 'kontra indikasi obat', 'mengantuk \\\\\\\\\\\\\\\\nbbbbbbbbbbbbbbbbbbbbbbbbbb\\\\\\\\\\\\\\\\nccccccccccccccccccccccccccccccccccccccccccccccccccccccccc\\\\\\\\\\\\\\\\nddddddddddddddddddddddddddddddddddddddddddddd\\\\\\\\\\\\\\\\neeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'),
(25, 'Oskadon', 'Tablet (compressi)', '', '', 2, '3 x 1', 'diminum', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 'Komix', 'Pilulae (PIL)', '', '', 1, '2 x 1', 'diminum', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 'Faramex', 'Tablet (compressi)', '', '', 1, '1 x 1', 'diminum', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 'kombantrin', 'Kapsul (capsule)', '2 gram', '20', 50, '3X1', 'Diminum dipagi hari', 'stabil', NULL, 'adehafidudin@gmail.com', '18-Sep-2020 03:30:56', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` bigint(20) NOT NULL,
  `activity` varchar(250) DEFAULT NULL,
  `created_at` varchar(30) DEFAULT NULL,
  `creator` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `activity`, `created_at`, `creator`) VALUES
(1, 'Mengubah Data Klinis', '15-Sep-2020 10:30:45', 'awalmuhibhalim@gmail.com'),
(2, 'Mengubah Data Klinis', '15-Sep-2020 10:30:42', 'awalmuhibhalim@gmail.com'),
(3, 'Melakukan Login', '15-Sep-2020 10:30:36', 'adehafidudin@gmail.com'),
(4, 'Melakukan Login', '16-Sep-2020 04:30:50', 'adehafidudin@gmail.com'),
(5, 'Melakukan Login', '16-Sep-2020 04:30:50', 'adehafidudin@gmail.com'),
(6, 'Melakukan Login', '16-Sep-2020 04:30:04', 'adehafidudin@gmail.com'),
(7, 'Melakukan Login', '16-Sep-2020 09:30:55', 'adehafidudin@gmail.com'),
(8, 'Melakukan Login', '16-Sep-2020 09:30:49', 'adehafidudin@gmail.com'),
(9, 'Melakukan Login', '17-Sep-2020 04:30:30', 'adehafidudin@gmail.com'),
(10, 'Melakukan Login', '17-Sep-2020 08:30:52', 'adehafidudin@gmail.com'),
(11, 'Melakukan Login', '17-Sep-2020 08:30:53', 'adehafidudin@gmail.com'),
(12, 'Mengubah Data Administrasi', '17-Sep-2020 09:30:48', 'adehafidudin@gmail.com'),
(13, 'Mengubah Data Administrasi', '17-Sep-2020 09:30:12', 'adehafidudin@gmail.com'),
(14, 'Mengubah Data Administrasi', '17-Sep-2020 09:30:26', 'adehafidudin@gmail.com'),
(15, 'Menambahkan/Mengubah Data Kombinasi obat', '17-Sep-2020 10:30:44', 'adehafidudin@gmail.com'),
(16, 'Menambahkan/Mengubah Data Kombinasi obat', '17-Sep-2020 10:30:38', 'adehafidudin@gmail.com'),
(17, 'Menambahkan/Mengubah Data Kombinasi obat', '17-Sep-2020 10:30:00', 'adehafidudin@gmail.com'),
(18, 'Menambahkan/Mengubah Data Kombinasi obat', '17-Sep-2020 10:30:10', 'adehafidudin@gmail.com'),
(19, 'Menambahkan/Mengubah Data Kombinasi obat', '17-Sep-2020 10:30:05', 'adehafidudin@gmail.com'),
(20, 'Menambahkan/Mengubah Data Kombinasi obat', '17-Sep-2020 10:30:24', 'adehafidudin@gmail.com'),
(21, 'Melakukan Login', '17-Sep-2020 11:30:34', 'adehafidudin@gmail.com'),
(22, 'Mengubah Data Administrasi', '17-Sep-2020 11:30:42', 'adehafidudin@gmail.com'),
(23, 'Mengubah Data Administrasi', '17-Sep-2020 11:30:31', 'adehafidudin@gmail.com'),
(24, 'Mengubah Data Administrasi', '17-Sep-2020 11:30:05', 'adehafidudin@gmail.com'),
(25, 'Mengubah Data Administrasi', '17-Sep-2020 11:30:04', 'adehafidudin@gmail.com'),
(26, 'Mengubah Data Administrasi', '17-Sep-2020 11:30:27', 'adehafidudin@gmail.com'),
(27, 'Mengubah Data Klinis', '18-Sep-2020 01:30:07', 'adehafidudin@gmail.com'),
(28, 'Menambahkan Data Klinis', '18-Sep-2020 02:30:26', 'adehafidudin@gmail.com'),
(29, 'Mengubah Data Farmasetis', '18-Sep-2020 03:30:56', 'adehafidudin@gmail.com'),
(30, 'Menambahkan Data User', '18-Sep-2020 04:30:36', 'adehafidudin@gmail.com'),
(31, 'Menambahkan/Mengubah Data Kombinasi obat', '18-Sep-2020 04:30:16', 'adehafidudin@gmail.com'),
(32, 'Menambahkan/Mengubah Data Kombinasi obat', '18-Sep-2020 04:30:18', 'adehafidudin@gmail.com'),
(33, 'Menambahkan/Mengubah Data Kombinasi obat', '18-Sep-2020 04:30:02', 'adehafidudin@gmail.com'),
(34, 'Menambahkan Data User', '18-Sep-2020 04:30:32', 'adehafidudin@gmail.com'),
(35, 'Menambahkan Data User', '18-Sep-2020 04:30:46', 'adehafidudin@gmail.com'),
(36, 'Mengubah Data User', '18-Sep-2020 04:30:49', 'adehafidudin@gmail.com'),
(37, 'Mengubah Data User', '18-Sep-2020 04:30:14', 'adehafidudin@gmail.com'),
(38, 'Mengubah Data User', '18-Sep-2020 04:30:49', 'adehafidudin@gmail.com'),
(39, 'Mengubah Data User', '18-Sep-2020 05:30:35', 'adehafidudin@gmail.com'),
(40, 'Mengubah Data User', '18-Sep-2020 05:30:08', 'adehafidudin@gmail.com'),
(41, 'Mengubah Data User', '18-Sep-2020 05:30:17', 'adehafidudin@gmail.com'),
(42, 'Mengubah Data User', '18-Sep-2020 05:30:34', 'adehafidudin@gmail.com'),
(43, 'Menambahkan Data User', '18-Sep-2020 05:30:56', 'adehafidudin@gmail.com'),
(44, 'Melakukan Login', '18-Sep-2020 09:30:05', 'awalmuhibhalim@gmail.com'),
(45, 'Melakukan Login', '19-Sep-2020 08:30:31', 'adehafidudin@gmail.com'),
(46, 'Mengubah Data Klinis', '19-Sep-2020 08:30:18', 'adehafidudin@gmail.com'),
(47, 'Mengubah Data Klinis', '19-Sep-2020 08:30:00', 'adehafidudin@gmail.com'),
(48, 'Mengubah Data Klinis', '19-Sep-2020 08:30:47', 'adehafidudin@gmail.com'),
(49, 'Melakukan Login', '19-Sep-2020 08:30:39', 'awalmuhibhalim@gmail.com'),
(50, 'Mengubah Data User', '19-Sep-2020 08:30:08', 'awalmuhibhalim@gmail.com'),
(51, 'Mengubah Data User', '19-Sep-2020 08:30:24', 'awalmuhibhalim@gmail.com'),
(52, 'Mengubah Data User', '19-Sep-2020 08:30:43', 'awalmuhibhalim@gmail.com'),
(53, 'Mengubah Data User', '19-Sep-2020 09:30:18', 'awalmuhibhalim@gmail.com'),
(54, 'Melakukan Login', '19-Sep-2020 09:30:40', 'adehafidudin@gmail.com'),
(55, 'Mengubah Data User', '19-Sep-2020 09:30:02', 'adehafidudin@gmail.com'),
(56, 'Melakukan Login', '19-Sep-2020 09:30:27', 'adehafidudin@gmail.com'),
(57, 'Menghapus Data User', '19-Sep-2020 09:30:54', 'adehafidudin@gmail.com'),
(58, 'Menghapus Data User', '19-Sep-2020 09:30:19', 'adehafidudin@gmail.com'),
(59, 'Menghapus Data User', '19-Sep-2020 09:30:45', 'adehafidudin@gmail.com'),
(60, 'Menghapus Data User wawan@mail.com(Staff)', '19-Sep-2020 09:30:51', 'adehafidudin@gmail.com'),
(61, 'Menghapus Data Klinis', '19-Sep-2020 10:30:07', 'adehafidudin@gmail.com'),
(62, 'Menghapus Data Klinis', '19-Sep-2020 10:30:24', 'adehafidudin@gmail.com'),
(63, 'Menghapus Data Klinis', '19-Sep-2020 10:30:20', 'adehafidudin@gmail.com'),
(64, 'Menghapus Data Klinis', '19-Sep-2020 10:30:32', 'adehafidudin@gmail.com'),
(65, 'Menghapus Data Klinis', '19-Sep-2020 10:30:34', 'adehafidudin@gmail.com'),
(66, 'Menghapus Data Klinis', '19-Sep-2020 10:30:11', 'adehafidudin@gmail.com'),
(67, 'Melakukan Login', '20-Sep-2020 01:30:38', 'adehafidudin@gmail.com'),
(68, 'Melakukan Login', '20-Sep-2020 03:30:14', 'adehafidudin@gmail.com'),
(69, 'Mengubah Data Farmasetis', '20-Sep-2020 05:30:36', 'adehafidudin@gmail.com'),
(70, 'Menghapus Data Klinis', '20-Sep-2020 05:30:25', 'adehafidudin@gmail.com'),
(71, 'Melakukan Login', '20-Sep-2020 08:30:23', 'adehafidudin@gmail.com'),
(72, 'Melakukan Login', '21-Sep-2020 02:30:33', 'awalmuhibhalim@gmail.com'),
(73, 'Melakukan Login', '21-Sep-2020 02:30:45', 'adehafidudin@gmail.com'),
(74, 'Mengubah Data Farmasetis', '21-Sep-2020 03:30:56', 'adehafidudin@gmail.com'),
(75, 'Mengubah Data Farmasetis', '21-Sep-2020 03:30:33', 'adehafidudin@gmail.com'),
(76, 'Mengubah Data Farmasetis', '21-Sep-2020 04:30:05', 'adehafidudin@gmail.com'),
(77, 'Melakukan Login', '21-Sep-2020 04:30:07', 'adehafidudin@gmail.com'),
(78, 'Melakukan Login', '21-Sep-2020 08:30:10', 'adehafidudin@gmail.com'),
(79, 'Melakukan Login', '21-Sep-2020 08:30:19', 'adehafidudin@gmail.com'),
(80, 'Menambahkan Data User', '21-09-2020 09:50:10', 'adehafidudin@gmail.com'),
(81, 'Mengubah Data Klinis', '21-09-2020 10:39:20', 'adehafidudin@gmail.com'),
(82, 'Melakukan Login', '21-09-2020 19:16:08', 'adehafidudin@gmail.com'),
(83, 'Menambahkan/Mengubah Data Kombinasi obat', '21-09-2020 19:23:05', 'adehafidudin@gmail.com'),
(84, 'Mengubah Data Klinis', '21-09-2020 19:40:49', 'adehafidudin@gmail.com'),
(85, 'Mengubah Data Klinis', '21-09-2020 19:41:12', 'adehafidudin@gmail.com'),
(86, 'Mengubah Data Klinis', '21-09-2020 19:45:45', 'adehafidudin@gmail.com'),
(87, 'Mengubah Data Klinis', '21-09-2020 19:48:53', 'adehafidudin@gmail.com'),
(88, 'Mengubah Data Farmasetis', '21-09-2020 19:54:53', 'adehafidudin@gmail.com'),
(89, 'Mengubah Data Farmasetis', '21-09-2020 19:55:03', 'adehafidudin@gmail.com'),
(90, 'Mengubah Data Farmasetis', '21-09-2020 19:55:17', 'adehafidudin@gmail.com'),
(91, 'Mengubah Data User', '21-09-2020 19:55:49', 'adehafidudin@gmail.com'),
(92, 'Mengubah Data Klinis', '21-09-2020 20:19:02', 'adehafidudin@gmail.com'),
(93, 'Menambahkan Data Klinis', '21-09-2020 20:59:41', 'adehafidudin@gmail.com'),
(94, 'Mengubah Data Klinis', '21-09-2020 21:24:40', 'adehafidudin@gmail.com'),
(95, 'Mengubah Data Klinis', '21-09-2020 21:25:56', 'adehafidudin@gmail.com'),
(96, 'Menambahkan Data Klinis', '22-09-2020 10:12:23', 'adehafidudin@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `klinis`
--

CREATE TABLE `klinis` (
  `id` int(11) NOT NULL,
  `administrasi_id` int(11) DEFAULT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `creator` varchar(50) DEFAULT NULL,
  `updated_at` varchar(50) DEFAULT NULL,
  `shift` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `klinis`
--

INSERT INTO `klinis` (`id`, `administrasi_id`, `created_at`, `creator`, `updated_at`, `shift`) VALUES
(23, 3, NULL, 'awalmuhibhalim@gmail.com', '15-Sep-2020 10:30:07', NULL),
(24, 3, NULL, 'awalmuhibhalim@gmail.com', '15-Sep-2020 10:30:45', NULL),
(28, 3, NULL, NULL, NULL, NULL),
(29, 3, NULL, NULL, NULL, NULL),
(30, 3, NULL, NULL, NULL, NULL),
(32, 3, NULL, NULL, NULL, NULL),
(33, 3, NULL, NULL, NULL, NULL),
(34, 3, NULL, NULL, NULL, NULL),
(35, 3, NULL, NULL, NULL, NULL),
(36, 3, '18-Sep-2020 01:30:07', NULL, NULL, NULL),
(37, 3, '18-Sep-2020 01:30:07', 'adehafidudin@gmail.com', '18-Sep-2020 01:30:07', NULL),
(39, 3, '18-Sep-2020 02:30:26', 'adehafidudin@gmail.com', '19-Sep-2020 08:30:47', NULL),
(40, 3, '19-Sep-2020 09:30:19', 'adehafidudin@gmail.com', NULL, NULL),
(41, 28, '20-Sep-2020 11:30:16', 'adehafidudin@gmail.com', '20-Sep-2020 11:30:34', NULL),
(42, 28, '21-09-2020 09:59:29', 'adehafidudin@gmail.com', '21-09-2020 10:19:21', 'shift siang'),
(43, 28, '21-09-2020 10:21:58', 'adehafidudin@gmail.com', '21-09-2020 21:25:56', 'shift malam'),
(44, 1, '21-09-2020 20:59:41', 'adehafidudin@gmail.com', NULL, 'shift pagi'),
(45, 28, '22-09-2020 10:12:23', 'adehafidudin@gmail.com', NULL, 'shift pagi');

-- --------------------------------------------------------

--
-- Table structure for table `kombinasi_obat`
--

CREATE TABLE `kombinasi_obat` (
  `id` int(11) NOT NULL,
  `farmasetis_x` int(11) DEFAULT NULL,
  `farmasetis_y` int(11) DEFAULT NULL,
  `interaksi_obat` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kombinasi_obat`
--

INSERT INTO `kombinasi_obat` (`id`, `farmasetis_x`, `farmasetis_y`, `interaksi_obat`) VALUES
(14, 25, 24, 'Minor'),
(15, 25, 26, 'Minor'),
(16, 25, 27, 'Indikasi'),
(17, 26, 24, 'Minor'),
(18, 26, 25, 'Minor'),
(19, 26, 27, 'Minor'),
(20, 27, 24, 'Minor'),
(21, 27, 25, 'Minor'),
(22, 27, 26, 'Minor'),
(23, 28, 24, 'Moderat'),
(24, 28, 25, 'Indikasi'),
(25, 28, 26, 'Indikasi'),
(26, 28, 27, 'Minor'),
(27, 25, 28, 'Minor'),
(28, 26, 28, 'Minor'),
(29, 27, 28, 'Minor'),
(31, 24, 26, 'Minor'),
(32, 24, 27, 'Indikasi');

-- --------------------------------------------------------

--
-- Table structure for table `obat`
--

CREATE TABLE `obat` (
  `id` int(11) NOT NULL,
  `klinis_id` int(11) DEFAULT NULL,
  `farmasetis_id` int(11) DEFAULT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `creator` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `resep_obat`
--

CREATE TABLE `resep_obat` (
  `id` bigint(20) NOT NULL,
  `klinis_id` int(11) DEFAULT NULL,
  `farmasetis_id` int(11) DEFAULT NULL,
  `bentuk_sediaan` varchar(100) DEFAULT NULL,
  `aturan_pakai` varchar(250) DEFAULT NULL,
  `jumlah_obat` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `resep_obat`
--

INSERT INTO `resep_obat` (`id`, `klinis_id`, `farmasetis_id`, `bentuk_sediaan`, `aturan_pakai`, `jumlah_obat`) VALUES
(16, 44, 27, 'Tablet (compressi)', '1 x 1', '3'),
(17, 44, 26, 'Pilulae (PIL)', '3 x 1', '6'),
(21, 43, 26, 'Imunoserum', '1 x 2', '8'),
(22, 43, 27, 'Tablet (compressi)', '1 x 2', '2'),
(23, 43, 25, 'Suppositoria', '3 x 1', '4'),
(24, 45, 27, 'Tablet (compressi)', '1 x 1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `display_name` varchar(200) DEFAULT NULL,
  `role` varchar(30) NOT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `creator` varchar(50) DEFAULT NULL,
  `updated_at` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `display_name`, `role`, `created_at`, `creator`, `updated_at`) VALUES
(1, 'adehafidudin@gmail.com', 'password', 'Ade Hafidudin', 'Manager', NULL, 'adehafidudin@gmail.com', '19-Sep-2020 09:30:02'),
(3, 'cantik@gmail.com', 'asadfs', 'Cantik', 'Manager', NULL, 'awalmuhibhalim@gmail.com', '19-Sep-2020 08:30:24'),
(4, 'adehafidudin@gmail.com', 'password', 'Ade Hafidudin', 'Staff', NULL, 'awalmuhibhalim@gmail.com', '14-Sep-2020 10:30:09'),
(7, 'awalmuhibhalim@gmail.com', 'password', 'sdfsdf', 'Staff', NULL, 'adehafidudin@gmail.com', '15-Sep-2020 04:30:15'),
(20, 'abcd@mail.com', 'password', 'abcd 1234', 'Staff', '18-Sep-2020 04:30:32', 'adehafidudin@gmail.com', '21-Sep-2020 07:30:49'),
(22, 'testawa@gmail.com', 'asadfs', 'abcd', 'Staff', '18-Sep-2020 05:30:56', 'adehafidudin@gmail.com', NULL),
(23, 'yayang@mail.com', 'password', 'Yayang', 'Manager', '21-Sep-2020 09:30:10', 'adehafidudin@gmail.com', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `administrasi`
--
ALTER TABLE `administrasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `farmasetis`
--
ALTER TABLE `farmasetis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `klinis`
--
ALTER TABLE `klinis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_ADMINISTRASI` (`administrasi_id`);

--
-- Indexes for table `kombinasi_obat`
--
ALTER TABLE `kombinasi_obat`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kombinasi` (`farmasetis_x`,`farmasetis_y`),
  ADD UNIQUE KEY `kombinasi_y` (`farmasetis_y`,`farmasetis_x`);

--
-- Indexes for table `obat`
--
ALTER TABLE `obat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `klinis_id` (`klinis_id`),
  ADD KEY `farmasetis_id` (`farmasetis_id`);

--
-- Indexes for table `resep_obat`
--
ALTER TABLE `resep_obat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `klinis_id` (`klinis_id`),
  ADD KEY `farmasetis_id` (`farmasetis_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `administrasi`
--
ALTER TABLE `administrasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `farmasetis`
--
ALTER TABLE `farmasetis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `klinis`
--
ALTER TABLE `klinis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `kombinasi_obat`
--
ALTER TABLE `kombinasi_obat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `obat`
--
ALTER TABLE `obat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `resep_obat`
--
ALTER TABLE `resep_obat`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `klinis`
--
ALTER TABLE `klinis`
  ADD CONSTRAINT `FK_ADMINISTRASI` FOREIGN KEY (`administrasi_id`) REFERENCES `administrasi` (`id`);

--
-- Constraints for table `kombinasi_obat`
--
ALTER TABLE `kombinasi_obat`
  ADD CONSTRAINT `kombinasi_obat_ibfk_1` FOREIGN KEY (`farmasetis_x`) REFERENCES `farmasetis` (`id`),
  ADD CONSTRAINT `kombinasi_obat_ibfk_2` FOREIGN KEY (`farmasetis_y`) REFERENCES `farmasetis` (`id`);

--
-- Constraints for table `obat`
--
ALTER TABLE `obat`
  ADD CONSTRAINT `obat_ibfk_1` FOREIGN KEY (`klinis_id`) REFERENCES `klinis` (`id`),
  ADD CONSTRAINT `obat_ibfk_2` FOREIGN KEY (`farmasetis_id`) REFERENCES `farmasetis` (`id`);

--
-- Constraints for table `resep_obat`
--
ALTER TABLE `resep_obat`
  ADD CONSTRAINT `resep_obat_ibfk_1` FOREIGN KEY (`klinis_id`) REFERENCES `klinis` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `resep_obat_ibfk_2` FOREIGN KEY (`farmasetis_id`) REFERENCES `farmasetis` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
