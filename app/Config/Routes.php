<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->post('/insertAbout', 'AboutController::insertAbout');
$routes->get('/form_about', 'AboutController::index');

$routes->get('/index', 'Home::index');
$routes->get('/halaman2', 'Home::halaman2');
$routes->get('/productlist', 'ProductController::index');

$routes->get('/userlist', 'UserController::index');
$routes->get('/form_add_user', 'UserController::openForm');
$routes->post('/adduser', 'UserController::save');
$routes->post('/user_datatable', 'UserController::view');
$routes->get('/edituser/(:num)', 'UserController::openFormEdit/$1');
$routes->post('/updateuser/(:num)', 'UserController::update/$1');
$routes->get('/deleteuser/(:num)', 'UserController::delete/$1');

$routes->get('/administrasilist', 'AdministrasiController::index');
$routes->get('/form_add_administrasi', 'AdministrasiController::openForm');
$routes->post('/addadministrasi', 'AdministrasiController::save');
$routes->post('/administrasi_datatable', 'AdministrasiController::view');
$routes->get('/editadministrasi/(:num)', 'AdministrasiController::openFormEdit/$1');
$routes->post('/updateadministrasi/(:num)', 'AdministrasiController::update/$1');
$routes->get('/deleteadministrasi/(:num)', 'AdministrasiController::delete/$1');
$routes->get('/findAdministration/(:any)', 'AdministrasiController::showAdministrasionList/$1');

$routes->get('/farmasetis_list', 'FarmasetisController::index');
$routes->get('/form_add_farmasetis', 'FarmasetisController::openForm');
$routes->post('/add_farmasetis', 'FarmasetisController::save');
$routes->post('/farmasetis_datatable', 'FarmasetisController::view');
$routes->get('/edit_farmasetis/(:num)', 'FarmasetisController::openFormEdit/$1');
$routes->post('/update_farmasetis/(:num)', 'FarmasetisController::update/$1');
$routes->get('/delete_farmasetis/(:num)', 'FarmasetisController::delete/$1');
$routes->get('/findAllFarmasetis', 'FarmasetisController::findAllFarmasetis');

$routes->get('/klinis_list', 'KlinisController::index');
$routes->get('/form_add_klinis', 'KlinisController::openForm');
$routes->post('/add_klinis', 'KlinisController::save');
$routes->post('/klinis_datatable', 'KlinisController::view');
$routes->get('/edit_klinis/(:num)', 'KlinisController::openFormEdit/$1');
$routes->post('/update_klinis/(:num)', 'KlinisController::update/$1');
$routes->get('/delete_klinis/(:num)', 'KlinisController::delete/$1');
$routes->post('/insertKlinis', 'KlinisController::insertKlinis');
$routes->post('/updateKlinis/(:num)', 'KlinisController::updateKlinis/$1');

$routes->get('/kombinasi_list', 'KombinasiController::index');
$routes->get('/form_add_kombinasi', 'KombinasiController::openForm');
$routes->post('/add_kombinasi', 'KombinasiController::save');
$routes->post('/kombinasi_datatable', 'KombinasiController::view');
$routes->get('/edit_kombinasi/(:num)', 'KombinasiController::openFormEdit/$1');
$routes->post('/update_kombinasi/(:num)', 'KombinasiController::update/$1');
$routes->get('/delete_kombinasi/(:num)/(:num)', 'KombinasiController::delete/$1/$2');
$routes->get('/findById', 'KombinasiController::detailKombinasi');
$routes->post('/insertMultipleKombinasi', 'KombinasiController::insertMultiple');

$routes->get('/history_list', 'HistoryController::index');
$routes->post('/history_datatable', 'HistoryController::view');

$routes->get('/login', 'LoginController::index');
$routes->post('/process_login', 'LoginController::login');
$routes->get('/logout', 'LoginController::logout');

$routes->get('/report_klinis_list', 'KlinisController::reportKlinis');
$routes->post('/report_klinis_datatable', 'KlinisController::viewReport');
$routes->get('/find_klinis_id/(:num)', 'KlinisController::reportDetail/$1');

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}