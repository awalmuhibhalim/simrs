<div class="row">
    <div class="col-md-12">
        <!-- Advanced Tables -->

        <div class="panel panel-default">
            <div class="panel-heading">
            </div>

            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tbl_list">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NRM</th>
                                <th>Pasien</th>
                                <th>No Telepon Pasien</th>
                                <th>Dokter</th>
                                <th>Tgl Resep</th>
                                <th>Tanggal Input</th>
                                <th>Petugas</th>
                                <th>Shift</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modal-xl">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Detail</h4>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                        <div class="row mb-4">
                            <div class="col-sm-2">
                                <div>
                                    <strong>Tanggal Resep</strong>
                                </div><br>
                                <div>
                                    <strong>Nama Pasien</strong>
                                </div>
                                <div>
                                    <strong>No. Tlpn Pasien</strong>
                                </div>
                                <div>
                                    <strong>Nama Dokter</strong>
                                </div>
                                <div>
                                    <strong>NRM</strong>
                                </div>
                                <div>
                                    <strong>Dibuat Pada</strong>
                                </div>
                                <div>
                                    <strong>Dibuat Oleh</strong>
                                </div>
                                <div>
                                    <strong>Shift Kerja</strong>
                                </div>
                                <div>
                                    <strong>Diubah Pada</strong>
                                </div>
                                <div>
                                    <strong>Jaminan</strong>
                                </div><br>
                            </div>
                            <div class="col-sm-1">
                                <div>
                                    <strong>:</strong>
                                </div><br>
                                <div>
                                    :
                                </div>
                                <div>
                                    :
                                </div>
                                <div>
                                    :
                                </div>
                                <div>
                                    :
                                </div>
                                <div>
                                    :
                                </div>
                                <div>
                                    :
                                </div>
                                <div>
                                    :
                                </div>
                                <div>
                                    :
                                </div>
                                <div>
                                    :
                                </div><br>
                            </div>
                            <div class="col-sm-4">
                                <div>
                                    <strong id="inv_tanggal_resep">#</strong>
                                </div><br>
                                <div><strong id="inv_pasien">-</strong></div>
                                <div><strong id="inv_no_tlpn_pasien">-</strong></div>
                                <div><strong id="inv_dokter">-</strong></div>
                                <div><strong id="inv_nrm">-</strong></div>
                                <div><strong id="inv_created_at">-</strong></div>
                                <div><strong id="inv_created_by">-</strong></div>
                                <div><strong id="inv_shift">-</strong></div>
                                <div><strong id="inv_updated_at">-</strong></div>
                                <div><strong id="inv_jaminan">-</strong></div>
                            </div>
                        </div>
                        <div class="row mb-4">
                            <div class="col-sm-12">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th colspan="3"><label for="exampleInputEmail1">Resep Obat</label></th>
                                            </tr>
                                            <tr>
                                                <th class="center">#</th>
                                                <th>Nama Obat</th>
                                                <th>Bentuk Sediaan</th>
                                                <th class="right">Jumlah Obat</th>
                                                <th class="right">Aturan Pakai</th>
                                            </tr>
                                        </thead>
                                        <tbody id="inv_obat">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <div type="button" class="btn btn-default" data-dismiss="modal">Close</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php base_url() ?>template/assets/js/jquery-1.10.2.js"></script>
<script>
$(document).ready(function() {
    let table = null;
    setTimeout(function() {
        table = $('#tbl_list').DataTable({
            "processing": true,
            "serverSide": true,
            "ordering": true,
            "order": [
                [0, 'asc']
            ],
            "ajax": {
                "url": "<?= base_url('report_klinis_datatable') ?>",
                "type": "POST"
            },
            "deferRender": true,
            "aLengthMenu": [
                [10, 20, 50],
                [10, 20, 50]
            ],
            "columns": [{
                    "render": function(data, type, row) {
                        return "";
                    }
                }, {
                    "data": "no_rekam_medis"
                }, {
                    "data": "nama_pasien"
                },
                {
                    "data": "no_telepon_pasien"
                },
                {
                    "data": "nama_dokter"
                },
                {
                    "data": "tanggal_resep"
                },
                {
                    "data": "created_at"
                },
                {
                    "data": "creator"
                },
                {
                    "data": "shift"
                },
                {
                    "render": function(data, type, row) { // Tampilkan kolom aksi
                        var html = '<button onclick="showData(\'' + row.id +
                            '\')" class="btn btn-primary">View</button>';
                        return html
                    }
                },
            ],
        });

        table.on('order.dt search.dt', function() {
            let length = table.page.info().length;
            let currentPage = table.page.info().page + 1;
            let nomor = 1 + (length * (currentPage - 1));
            table.column(0, {
                search: 'applied',
                order: 'applied',
            }).nodes().each(function(cell, i) {
                let loop = i + 1;
                let num = nomor + loop - 1;
                cell.innerHTML = num;
            });
        }).draw();
    }, 1000)
});

let drags = [];
let timeout;

function showData(id) {
    drags = [];
    $.ajax({
        method: "GET",
        url: "find_klinis_id/" + id,
        contentType: "application/json",
    }).done(function(response) {
        let data = JSON.parse(response)
        console.log(data)
        $('#modal-xl').modal('show');
        $('#inv_tanggal_resep').text(data.klinis.tanggal_resep);
        $('#inv_pasien').text(data.klinis.nama_pasien);
        $('#inv_no_tlpn_pasien').text(data.klinis.no_telepon_pasien);
        $('#inv_dokter').text(data.klinis.nama_dokter);
        $('#inv_nrm').text(data.klinis.no_rekam_medis);
        $('#inv_created_at').text(data.klinis.created_at);
        $('#inv_created_by').text(data.klinis.creator);
        $('#inv_shift').text(data.klinis.shift);
        $('#inv_updated_at').text(data.klinis.updated_at);
        $('#inv_jaminan').text(data.klinis.jenis_penjamin);

        let resep = [];
        resep = data.resep;
        for (let i = 0; i < resep.length; i++) {
            selectFarmasetis(resep[i].farmasetis_id, resep[i].nama_obat, resep[i].bentuk_sediaan, resep[i]
                .jumlah_obat, resep[i].aturan_pakai)
        }
    })
}

function selectFarmasetis(id, nama_obat, bentuk_sediaan, jumlah_obat, aturan_pakai) {
    setTimeout(function() {
        $('#farmasetis_id').empty();
    }, 2000);
    let flag = false;
    let obj = {
        'id': id,
        'nama_obat': nama_obat,
        'bentuk_sediaan': bentuk_sediaan,
        'jumlah_obat': jumlah_obat,
        'aturan_pakai': aturan_pakai

    }
    for (let i = 0; i < drags.length; i++) {
        if (drags[i].id == id) {
            toastr.warning(nama_obat + " sudah tersedia");
            flag = true;
            break;
        }
    }

    if (!flag) {
        drags.push(obj);
        drawFarmasetis()
    }
}

function drawFarmasetis() {

    $('#inv_obat').empty();
    for (let i = 0; i < drags.length; i++) {
        let no = i + 1;
        let optionValue = drags[i].bentuk_sediaan;

        let tr_obat = '<tr>\n\
                            <td class="center">' + no + '</td>\n\
                            <td class="left strong">' + drags[i].nama_obat + '</td>\n\
                            <td class="left">' + optionValue + '</td>\n\
                            <td class="right">' + drags[i].jumlah_obat + '</td>\n\
                            <td class="right">' + drags[i].aturan_pakai + '</td>\n\
                        </tr>';
        $('#inv_obat').append(tr_obat);
    }
}
</script>