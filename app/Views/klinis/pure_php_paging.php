<div class="row">
    <div class="col-md-12">
        <!-- Advanced Tables -->

        <div class="panel panel-default">
            <div class="panel-heading">
                <!--                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-xl">
                    Add User
                </button><br>-->
            </div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-md-8">
                        <div type="button" class="btn btn-primary" onclick="openform()">Tambah
                            Data</div>
                    </div>
                    <div class="col-md-4">
                        <form action="" method="get">
                            <div class="input-group">
                                <input id="btn-input" type="text" class="form-control input-sm" name="keyword"
                                    placeholder="Search...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default btn-sm" id="btn-chat" type="submit">
                                        Search
                                    </button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
                <br>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Indikasi Obat</th>
                                <th>Dosis Obat</th>
                                <th>Rute Pemberian Obat</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1 + (5 * ($currentPage - 1));
                            foreach ($klinis as $key => $value) {
                            ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $value['indikasi_obat']; ?></td>
                                <td><?= $value['dosis_obat']; ?></td>
                                <td><?= $value['rute_pemberian_obat']; ?></td>
                                <td><button onclick="showData(<?= $value['id']; ?>)" class="btn btn-primary">View or
                                        Edit</button>&nbsp;&nbsp;&nbsp;
                                    <div class="btn btn-danger" onclick="deleteData(<?= $value['id']; ?>)">Delete</div>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?= $pager->links(); ?>
                </div>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tbl_list">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Pasien</th>
                                <th>Nomor Rekam Medis</th>
                                <th>Nomor Telpn Pasien</th>
                                <th>Indikasi Obat</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <!--End Advanced Tables -->
    </div>


    <div class="modal fade" id="modal-xl">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Form Klinis</h4>
                </div>
                <div class="modal-body">
                    <form id="form" role="form" action="<?= base_url('add_klinis') ?>" method="post">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Pasien</label>
                                <input type="text" class="form-control" id="search" placeholder="search..."
                                    onkeyup="getAdminstrasi()" />
                                <input type="hidden" class="form-control" id="administrasi_id" />
                                <table id="table_administrasi" class="table table-striped table-bordered table-hover"
                                    style="display:none">
                                    <tbody id="administrasi_list"></tbody>
                                </table>
                                <p id="nama_pasien"></p>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Obat</label>
                                <input type="text" class="form-control" id="search_farmasetis" placeholder="search..."
                                    onkeyup="getFarmasetis()" />
                                <table id="table_farmasetis" class="table table-striped table-bordered table-hover">
                                    <tbody id="farmasetis_id"></tbody>
                                </table>
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        </tr>
                                    </thead>
                                    <tbody id="farmasetis_selected"></tbody>
                                </table>
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                        </tr>
                                    </thead>
                                    <tbody id="comparation">
                                    </tbody>
                                </table>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Indikasi Obat</label>
                                <input type="text" name="indikasi_obat" id="indikasi_obat" class="form-control"
                                    placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Dosis Obat</label>
                                <input type="text" name="dosis_obat" id="dosis_obat" class="form-control"
                                    placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Rute Pemberian Obat</label>
                                <input type="text" name="rute_pemberian_obat" id="rute_pemberian_obat"
                                    class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Tepat Waktu</label>
                                <input type="text" name="tepat_waktu" id="tepat_waktu" class="form-control"
                                    placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Duplikasi</label>
                                <input type="text" name="duplikasi" id="duplikasi" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Alergi</label>
                                <input type="text" name="alergi" id="alergi" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Interaksi Obat</label>
                                <textarea name="interaksi_obat" id="interaksi_obat" class="form-control"
                                    placeholder=""></textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Kontraindikasi Obat</label>
                                <input type="text" name="kontraindikasi_obat" id="kontraindikasi_obat"
                                    class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Efek Samping</label>
                                <input type="text" name="efek_samping" id="efek_samping" class="form-control"
                                    placeholder="">
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </form>
                </div>
                <div class="modal-footer justify-content-between">
                    <div type="button" class="btn btn-default" data-dismiss="modal">Close</div>
                    <div type="button" class="btn btn-warning" onclick="reset()">Reset</div>
                    <div type="button" class="btn btn-primary" id="save" onclick="save()">Save</div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<script src="<?php base_url() ?>template/assets/js/jquery-1.10.2.js"></script>
<script>
$(document).ready(function() {
    let table = null;
    setTimeout(function() {
        table = $('#tbl_list').DataTable({
            "processing": true,
            "serverSide": true,
            "ordering": true, // Set true agar bisa di sorting
            "order": [
                [0, 'asc']
            ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
            "ajax": {
                "url": "<?= base_url('klinis_datatable') ?>", // URL file untuk proses select datanya
                "type": "POST"
            },
            "deferRender": true,
            "aLengthMenu": [
                [5, 10, 50],
                [5, 10, 50]
            ], // Combobox Limit
            "columns": [{
                    "render": function(data, type, row) {
                        return "";
                    }
                }, {
                    "data": "nama_pasien"
                },
                {
                    "data": "no_rekam_medis"
                },
                {
                    "data": "no_telepon_pasien"
                }, // Tampilkan alamat
                {
                    "data": "indikasi_obat"
                },
                {
                    "render": function(data, type, row) { // Tampilkan kolom aksi
                        var html = '<button onclick="showData(\'' + row.id +
                            '\')" class="btn btn-primary">View or Edit</button>';
                        html += '&nbsp;&nbsp;&nbsp<button onclick="deleteData(\'' + row
                            .id +
                            '\')" class="btn btn-danger">Delete</button>';
                        return html
                    }
                },
            ],
        });

        table.on('order.dt search.dt', function() {
            table.column(0, {
                search: 'applied',
                order: 'applied'
            }).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    }, 1000)
});

let drags = [];
let timeout;

function openform() {
    $('#modal-xl').modal('show');
    $('#save').attr('onclick', 'save()')
    reset()
    drags = [];
    drawFarmasetis()
    $('#comparation').empty();
}

function reset() {
    $('#form')[0].reset();
}

function save() {
    $('#save').attr('disabled', true);
    let resep = [];
    for (let i = 0; i < drags.length; i++) {
        let farmasetis_id = $('#farmasetis_id_' + i).val();
        let bentuk_sediaan = $('#bentuk_sediaan_' + i).val();
        let jumlah_obat = $('#jumlah_obat_' + i).val();
        let aturan_pakai = $('#aturan_pakai_' + i).val();
        let obj = {
            farmasetis_id: parseInt(farmasetis_id),
            bentuk_sediaan: bentuk_sediaan,
            jumlah_obat: jumlah_obat,
            aturan_pakai: aturan_pakai
        }
        resep.push(obj);
    }

    let value = {
        "administrasi_id": parseInt($('#administrasi_id').val()),
        "indikasi_obat": $('#indikasi_obat').val(),
        "dosis_obat": $('#dosis_obat').val(),
        "rute_pemberian_obat": $('#rute_pemberian_obat').val(),
        "tepat_waktu": $('#tepat_waktu').val(),
        "duplikasi": $('#duplikasi').val(),
        "alergi": $('#alergi').val(),
        "interaksi_obat": $('#interaksi_obat').val(),
        "kontraindikasi_obat": $('#kontraindikasi_obat').val(),
        "efek_samping": $('#efek_samping').val(),
        "resep_obat": resep
    }

    $.ajax({
        method: "POST",
        url: "insertKlinis",
        contentType: "json",
        data: JSON.stringify(value)
    }).done(function(response) {
        alert("success")
        $('#save').attr('disabled', false);
        location.reload();
    })
}

function showData(id) {
    drags = [];
    $.ajax({
        method: "GET",
        url: "edit_klinis/" + id,
        contentType: "application/json",
    }).done(function(response) {
        let data = JSON.parse(response)
        console.log(data)
        $('#modal-xl').modal('show');
        $('#save').attr('onclick', 'update(' + data.klinis.id + ')')
        $('#nama_pasien').text("Nama Pasien : " + data.klinis.nama_pasien);
        $('#indikasi_obat').val(data.klinis.indikasi_obat)
        $('#dosis_obat').val(data.klinis.dosis_obat);
        $('#rute_pemberian_obat').val(data.klinis.rute_pemberian_obat);
        $('#tepat_waktu').val(data.klinis.tepat_waktu);
        $('#duplikasi').val(data.klinis.duplikasi);
        $('#alergi').val(data.klinis.alergi);
        $('#interaksi_obat').val(data.klinis.interaksi_obat);
        $('#kontraindikasi_obat').val(data.klinis.kontraindikasi_obat);
        $('#efek_samping').val(data.klinis.efek_samping);
        let resep = [];
        resep = data.resep;
        for (let i = 0; i < resep.length; i++) {
            selectFarmasetis(resep[i].farmasetis_id, resep[i].nama_obat, resep[i].bentuk_sediaan, resep[i]
                .jumlah_obat, resep[i].aturan_pakai)
        }
    })
}

function update(id) {
    $('#save').attr('disabled', true);
    let resep = [];
    for (let i = 0; i < drags.length; i++) {
        let farmasetis_id = $('#farmasetis_id_' + i).val();
        let bentuk_sediaan = $('#bentuk_sediaan_' + i).val();
        let jumlah_obat = $('#jumlah_obat_' + i).val();
        let aturan_pakai = $('#aturan_pakai_' + i).val();
        let obj = {
            farmasetis_id: parseInt(farmasetis_id),
            bentuk_sediaan: bentuk_sediaan,
            jumlah_obat: jumlah_obat,
            aturan_pakai: aturan_pakai
        }
        resep.push(obj);
    }

    let value = {
        "administrasi_id": parseInt($('#administrasi_id').val()),
        "indikasi_obat": $('#indikasi_obat').val(),
        "dosis_obat": $('#dosis_obat').val(),
        "rute_pemberian_obat": $('#rute_pemberian_obat').val(),
        "tepat_waktu": $('#tepat_waktu').val(),
        "duplikasi": $('#duplikasi').val(),
        "alergi": $('#alergi').val(),
        "interaksi_obat": $('#interaksi_obat').val(),
        "kontraindikasi_obat": $('#kontraindikasi_obat').val(),
        "efek_samping": $('#efek_samping').val(),
        "resep_obat": resep
    }

    $.ajax({
        method: "POST",
        url: "updateKlinis/" + id,
        contentType: "json",
        data: JSON.stringify(value)
    }).done(function(response) {
        alert("success")
        $('#save').attr('disabled', false);
        // location.reload();
    })
}

function deleteData(id) {
    $.confirm({
        title: 'Delete data?',
        content: 'This dialog will automatically trigger \'cancel\' in 10 seconds if you don\'t respond.',
        autoClose: 'cancelAction|10000',
        buttons: {
            deleteAction: {
                text: 'delete user',
                action: function() {
                    $.ajax({
                        method: "GET",
                        url: "deleteuser/" + id,
                        contentType: "application/json"
                    }).done(function(response) {
                        $.alert('Deleted the user!');
                        location.reload();
                    })
                }
            },
            cancelAction: function() {
                $.alert('action is canceled');
            }
        }
    });

}

function getAdminstrasi() {
    let keyword = $('#search').val().trim();
    if (keyword.length >= 2) {
        clearTimeout(timeout)
        timeout = setTimeout(function() {
            $.ajax({
                method: "GET",
                url: "AdministrasiController/showAdministrasionList",
                contentType: "application/json",
                data: {
                    'keyword': keyword
                },
            }).done(function(response) {
                let data = JSON.parse(response);
                if (data == null) {
                    return
                }
                $('#table_administrasi').show();
                $('#administrasi_list').empty();
                let tr = '<tr>\n\
                        <th>No</th>\n\
                        <th>No Rekam Medis</th>\n\
                        <th>Nama Pasien</th>\n\
                        <th>Nama Dokter</th>\n\
                        <th></th>\n\
                    </tr>';
                $('#administrasi_list').append(tr);
                for (let i = 0; i < data.length; i++) {
                    let no = i + 1;
                    tr = '<tr>\n\
                            <td>' + no + '</td>\n\
                            <td>' + data[i].no_rekam_medis + '</td>\n\
                            <td>' + data[i].nama_pasien + '</td>\n\
                            <td>' + data[i].nama_dokter + '</td>\n\
                            <td><buttom class="btn btn-info" onclick="pilihPasien(\'' + data[i].id + '\',\'' + data[i]
                        .nama_pasien + '\')">Select</buttom>\n\
                            </td>\n\
                        </tr>';
                    $('#administrasi_list').append(tr);
                }
            })
        }, 1000)
    } else if (keyword.length < 3) {
        $('#table_administrasi').hide();
    }
}

function getFarmasetis() {
    let keyword = $('#search_farmasetis').val();
    if (keyword.length >= 2) {
        clearTimeout(timeout)
        timeout = setTimeout(function() {
            $.ajax({
                method: "GET",
                url: "FarmasetisController/serachFarmasetis",
                contentType: "application/json",
                data: {
                    'keyword': keyword
                },
            }).done(function(response) {
                let data = JSON.parse(response);
                if (data == null) {
                    return;
                }
                $('#table_farmasetis').show();
                $('#farmasetis_id').empty();
                let tr = '<tr>\n\
                        <th>No</th>\n\
                        <th>Nama Obat</th>\n\
                        <th>Satuan</th>\n\
                        <th>Aturan Pakai</th>\n\
                        <th></th>\n\
                    </tr>';
                $('#farmasetis_id').append(tr);
                for (let i = 0; i < data.length; i++) {
                    let no = i + 1;
                    tr = '<tr>\n\
                            <td>' + no + '</td>\n\
                            <td>' + data[i].nama_obat + '</td>\n\
                            <td>' + data[i].bentuk_sediaan + '</td>\n\
                            <td>' + data[i].aturan_pakai + '</td>\n\
                            <td><buttom class="btn btn-info" onclick="selectFarmasetis(\'' + data[i].id + '\',\'' +
                        data[i].nama_obat + '\',\'' +
                        data[i].bentuk_sediaan + '\',\'' +
                        data[i].jumlah_obat + '\',\'' +
                        data[i].aturan_pakai + '\')\n\
                ">Select</buttom>\n\
                            </td>\n\
                        </tr>';
                    $('#farmasetis_id').append(tr);
                }
            })
        }, 1000)
    } else if (keyword.length < 3) {
        $('#table_farmasetis').hide();
    }
}

function pilihPasien(id, nama_pasien) {
    let flag = false;
    let obj = {
        'id': id,
        'nama_pasien': nama_pasien
    }
    $('#nama_pasien').text("Nama Pasien : " + nama_pasien);
    $('#table_administrasi').hide();
    $('#administrasi_id').val(id);
}

function selectFarmasetis(id, nama_obat, bentuk_sediaan, jumlah_obat, aturan_pakai) {
    let flag = false;
    let obj = {
        'id': id,
        'nama_obat': nama_obat,
        'bentuk_sediaan': bentuk_sediaan,
        'jumlah_obat': jumlah_obat,
        'aturan_pakai': aturan_pakai

    }
    for (let i = 0; i < drags.length; i++) {
        if (drags[i].id == id) {
            alert(nama_obat + " already exist");
            flag = true;
            break;
        }
    }

    if (!flag) {
        drags.push(obj);
        drawFarmasetis()
        if (drags.length > 1) {
            comparation()
        }
    }
}

function drawFarmasetis() {
    $('#farmasetis_selected').empty();
    for (let i = 0; i < drags.length; i++) {
        let no = i + 1;
        let optionValue = drags[i].bentuk_sediaan;
        let tr = '<tr id="farmasetis_' + drags[i].id + '">\n\
                            <td>' + no + '</td>\n\
                            <td><input type="hidden" id="farmasetis_id_' + i + '" value="' + drags[i].id + '" ></td>\n\
                            <td>' + drags[i].nama_obat + '</td>\n\
                            <td>\n\
                                <select class="form-control" id="bentuk_sediaan_' + i + '">\n\
                                <option value="Pulvis (serbuk)">Pulvis (serbuk)</option>\n\
                                    <option value="Pulveres">Pulveres</option>\n\
                                    <option value="Tablet (compressi)">Tablet (compressi)</option>\n\
                                    <option value="Pilulae (PIL)">Pilulae (PIL)</option>\n\
                                    <option value="Kapsul (capsule)">Kapsul (capsule)</option>\n\
                                    <option value="Solutiones (Larutan)">Solutiones (Larutan)</option>\n\
                                    <option value="Suspensi">Suspensi</option>\n\
                                    <option value="Emulsi">Emulsi (elmusiones)</option>\n\
                                    <option value="Infusa">Infusa</option>\n\
                                    <option value="Imunoserum">Imunoserum (immunosera)</option>\n\
                                    <option value="Salep">Salep (unguenta)</option>\n\
                                    <option value="Suppositoria">Suppositoria</option>\n\
                                    <option value="Obat tetes">Obat tetes (guttae)</option>\n\
                                    <option value="Injeksi">Injeksi (injectiones)</option>\n\
                                    <option value="Aerosol">Aerosol</option>\n\
                                    <option value="Gel">Gel</option>\n\
                                    <option value="Ovula">Ovula</option>\n\
                                    <option value="Implan">Implan</option>\n\
                                    <option value="Obat Kumur (Gargle)">Obat Kumur (Gargle)</option>\n\
                                    <option value="Sirup">Sirup</option>\n\
                                    <option value="Elixir">Elixir</option>\n\
                                    <option value="potio">potio</option>\n\
                                </select>\n\
                            </td>\n\
                            <td><input type="text" class="form-control" id="jumlah_obat_' + i +
            '" placeholder="jumlah obat" value="' + drags[i].jumlah_obat + '"></td>\n\
                            <td><input type="text" class="form-control" id="aturan_pakai_' + i +
            '" placeholder="aturan pakai" value="' + drags[i].aturan_pakai + '"></td>\n\
                            <td><buttom class="btn btn-danger" onclick="deleteFarmasetis(\'' + drags[i].id + '\',\'' +
            drags[i].nama_obat + '\')\n\
                ">Remove</buttom>\n\
                            </td>\n\
                        </tr>';
        $('#farmasetis_selected').append(tr);
        $('#bentuk_sediaan_' + i).val(optionValue);
    }
}

function deleteFarmasetis(id, nama_obat) {
    for (let i = 0; i < drags.length; i++) {
        if (drags[i].id == id) {
            drags.splice(i, 1);
            $('#farmasetis_' + id).remove();
            drawFarmasetis()
            comparation()
            break;
        }
    }
}

function comparation() {
    $('#comparation').empty();
    let dragsTemp = [];
    dragsTemp = drags;
    let obatTemp = [];
    for (let i = 0; i < dragsTemp.length; i++) {
        for (let j = 0; j < drags.length; j++) {
            if (dragsTemp[i].id != drags[j].id) {

                let obj = {
                    'o1': dragsTemp[i].id,
                    'o2': drags[j].id
                }
                obatTemp.push(obj);
                for (let k = 0; k < obatTemp.length; k++) {
                    if (obatTemp[k].o1 == drags[j].id && obatTemp[k].o2 == dragsTemp[i].id) {
                        console.log(dragsTemp[i].nama_obat + " <  > " + drags[j].nama_obat)
                        interaksiObat(dragsTemp[i].id, drags[j].id);
                    }
                }
            }
        }
    }
}

function interaksiObat(obat_a, obat_b) {
    $.ajax({
        method: "GET",
        url: "KombinasiController/dragCompare",
        contentType: "application/json",
        data: {
            'obat_a': obat_a,
            'obat_b': obat_b
        }
    }).done(function(response) {
        let data = JSON.parse(response);
        if (data == null) {
            return;
        }
        console.log(data)
        let obat_a;
        let obat_b;
        let button = "";
        for (let i = 0; i < drags.length; i++) {
            if (drags[i].id == data.farmasetis_x) {
                obat_a = drags[i].nama_obat;
            }

            if (drags[i].id == data.farmasetis_y) {
                obat_b = drags[i].nama_obat;
            }
        }

        if (data.interaksi_obat == 'Major') {
            button = "<button class='btn' style='background-color:#ff0000; color:white'>Major</button>";
        } else if (data.interaksi_obat == 'Moderat') {
            button = "<button class='btn' style='background-color:#ff9999; color:white'>Moderat</button>";
        } else if (data.interaksi_obat == 'Minor') {
            button = "<button class='btn' style='background-color:#ffe6e6; color:black'>Minor</button>";
        } else if (data.interaksi_obat == 'Indikasi') {
            button = "<button class='btn' style='background-color:yellow; color:black'>Indikasi</button>";
        }

        let tag = '<tr>\n\
                    <th>' + obat_a + '</th>\n\
                    <th>&lt; &gt;</th>\n\
                    <th>' + obat_b + '</th>\n\
                    <th>' + button + '</th>\n\
                   </tr>'
        $('#comparation').append(tag)
    })
}

function submitKlinis() {
    let value = [];
    for (let i = 0; i < drags.length; i++) {
        let farmasetis_id = $('#farmasetis_id_' + i).val();
        let bentuk_sediaan = $('#bentuk_sediaan_' + i).val();
        let jumlah_obat = $('#jumlah_obat_' + i).val();
        let aturan_pakai = $('#aturan_pakai_' + i).val();
        let obj = {
            farmasetis_id: parseInt(farmasetis_id),
            bentuk_sediaan: bentuk_sediaan,
            jumlah_obat: jumlah_obat,
            aturan_pakai: aturan_pakai
        }
        value.push(obj);
    }

    $.ajax({
        method: "POST",
        url: "insertKlinis",
        contentType: "json",
        data: JSON.stringify({
            "administrasi_id": parseInt($('#administrasi_id').val()),
            "indikasi_obat": $('#indikasi_obat').val(),
            "dosis_obat": $('#dosis_obat').val(),
            "rute_pemberian_obat": $('#rute_pemberian_obat').val(),
            "tepat_waktu": $('#tepat_waktu').val(),
            "duplikasi": $('#duplikasi').val(),
            "alergi": $('#alergi').val(),
            "interaksi_obat": $('#interaksi_obat').val(),
            "kontraindikasi_obat": $('#kontraindikasi_obat').val(),
            "efek_samping": $('#efek_samping').val(),
            "resep_obat": value
        })
    }).done(function(response) {
        console.log(response)
    })
}
</script>