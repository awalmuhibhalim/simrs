<div class="row">
    <div class="col-md-12">
        <!-- Advanced Tables -->

        <div class="panel panel-default">
            <div class="panel-heading">
            </div>
            <div class="panel-body">
                <form role="form" action="<?= base_url('adduser');?>" method="post">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Username</label>
                                <input type="text" name="username" class="form-control" placeholder="Enter Username">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="password"  name="password" class="form-control" placeholder="Enter Password">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Display Name</label>
                                <input type="text" name="display_name" class="form-control"  placeholder="Enter display name">
                            </div>
                            <div class="form-group">
                            <label for="exampleInputPassword1">Role</label>
                            <select name="role" class="form-control" >
                                <option value="Staff">Staff</option>
                                <option value="Dokter">Dokter</option>
                            </select>
                        </div>
                            
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>

            </div>
        </div>
        <!--End Advanced Tables -->
    </div>
</div>

