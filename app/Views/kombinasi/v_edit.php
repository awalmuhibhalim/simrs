<div class="row">
    <div class="col-md-12">
        <!-- Advanced Tables -->

        <div class="panel panel-default">
            <div class="panel-heading">
            </div>
            <div class="panel-body">
            <form role="form" action="<?= base_url('update_kombinasi/'.$kombinasi['id'])?>" method="post">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Farmasetis X</label>
                                <input type="text" name="farmasetis_x" class="form-control" placeholder="" value="<?= $kombinasi['farmasetis_x'];?>">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Farmasetis Y</label>
                                <input type="text"  name="farmasetis_y" class="form-control" placeholder="" value="<?= $kombinasi['farmasetis_y'];?>">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Interaksi Obat</label>
                                <input type="text"  name="interaksi_obat" class="form-control" placeholder="" value="<?= $kombinasi['interaksi_obat'];?>">
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                </form>
            </div>
        </div>
        <!--End Advanced Tables -->
    </div>
</div>

