<div class="row">
    <div class="col-md-12">
        <!-- Advanced Tables -->

        <div class="panel panel-default">
            <div class="panel-heading">
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-8">
                        <!-- <div type="button" class="btn btn-primary" onclick="openform()">Tambah
                            Data</div> -->
                        <a href="<?= base_url('form_add_kombinasi') ?>" class="btn btn-primary">Tambah
                            Data</a>

                    </div>
                </div>
                <br>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tbl_list">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Obat</th>
                                <th>Bentuk Sediaan</th>
                                <th>Satuan</th>
                                <th>Jumlah</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <!--End Advanced Tables -->
    </div>


    <div class="modal fade" id="modal-xl">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Form Kombinasi</h4>
                </div>
                <div class="modal-body">
                    <form id="form" role="form" action="<?= base_url('add_kombinasi') ?>" method="post">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Pilih Obat</label>
                                <select class="form-control" id="kombinasi_list" onchange="getFarmasetisEx()">
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Kombinasi Obat :</label>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group" id="farmasetis_chekbox_list">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card-footer">
                            <!-- <div onclick="submit()" class="btn btn-primary">Submit</div> -->
                            <div onclick="submitMultipleCombination()" class="btn btn-primary">Submit</div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer justify-content-between">
                    <div type="button" class="btn btn-default" data-dismiss="modal">Close</div>
                    <div type="button" class="btn btn-warning" onclick="reset()">Reset</div>
                    <div type="button" class="btn btn-primary" id="save" onclick="save()">Save</div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal-list">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Kombinasi <b><span id="title"></span></b></h4>
                </div>
                <div class="modal-body custom_scroll">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>Interaksi Obat</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="list_obat">

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<script src="<?php base_url() ?>template/assets/js/jquery-1.10.2.js"></script>
<script>
$(document).ready(function() {
    let table = null;
    setTimeout(function() {
        table = $('#tbl_list').DataTable({
            "processing": true,
            "serverSide": true,
            "ordering": true, // Set true agar bisa di sorting
            "order": [
                [0, 'asc']
            ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
            "ajax": {
                "url": "<?= base_url('kombinasi_datatable') ?>", // URL file untuk proses select datanya
                "type": "POST"
            },
            "deferRender": true,
            "aLengthMenu": [
                [10, 20, 50],
                [10, 20, 50]
            ], // Combobox Limit
            "columns": [{
                    "render": function(data, type, row) {
                        return "";
                    }
                }, {
                    "data": "nama_obat"
                },
                {
                    "data": "bentuk_sediaan"
                },
                {
                    "data": "satuan_sediaan"
                },
                {
                    "data": "jumlah_obat"
                },
                {
                    "render": function(data, type, row) { // Tampilkan kolom aksi
                        var html = '<button onclick="showCombination(\'' + row.id +
                            '\')" class="btn btn-primary">Interaksi Obat</button>';
                        return html
                    }
                },
            ],
        });

        table.on('order.dt search.dt', function() {
            let length = table.page.info().length;
            let currentPage = table.page.info().page + 1;
            let nomor = 1 + (length * (currentPage - 1));
            table.column(0, {
                search: 'applied',
                order: 'applied',
            }).nodes().each(function(cell, i) {
                let loop = i + 1;
                let num = nomor + loop - 1;
                cell.innerHTML = num;
            });
        }).draw();
    }, 1000)
});

function openform() {
    $('#modal-xl').modal('show');
    $('#save').attr('onclick', 'save()')
    reset()
}

function reset() {
    $('#form')[0].reset();
}

function save() {
    $('#save').attr('disabled', true);
    let value = {
        "nama_obat": $('#nama_obat').val(),
        "bentuk_sediaan": $('#bentuk_sediaan').val(),
        "kekuatan_sediaan": $('#kekuatan_sediaan').val(),
        "satuan_sediaan": $('#satuan_sediaan').val(),
        "jumlah_obat": $('#jumlah_obat').val(),
        "aturan_pakai": $('#aturan_pakai').val(),
        "cara_pakai": $('#cara_pakai').val(),
        "stabilitas_obat": $('#stabilitas_obat').val()
    };

    $.ajax({
        method: "POST",
        url: "add_farmasetis",
        contentType: "application/json",
        data: JSON.stringify(value)
    }).done(function(response) {
        alert("success")
        $('#save').attr('disabled', false);
        location.reload();
    })
}

function showData(id) {
    $.ajax({
        method: "GET",
        url: "edit_farmasetis/" + id,
        contentType: "application/json",
    }).done(function(response) {
        let data = JSON.parse(response)
        console.log(data)
        $('#modal-xl').modal('show');
        $('#save').attr('onclick', 'update(' + data.id + ')')
        $('#nama_obat').val(data.nama_obat);
        $('#bentuk_sediaan').val(data.bentuk_sediaan);
        $('#kekuatan_sediaan').val(data.kekuatan_sediaan);
        $('#satuan_sediaan').val(data.satuan_sediaan);
        $('#jumlah_obat').val(data.jumlah_obat);
        $('#aturan_pakai').val(data.aturan_pakai);
        $('#cara_pakai').val(data.cara_pakai);
        $('#stabilitas_obat').val(data.stabilitas_obat);
    })
}

function update(id) {
    $('#save').attr('disabled', true);
    let value = {
        "nama_obat": $('#nama_obat').val(),
        "bentuk_sediaan": $('#bentuk_sediaan').val(),
        "kekuatan_sediaan": $('#kekuatan_sediaan').val(),
        "satuan_sediaan": $('#satuan_sediaan').val(),
        "jumlah_obat": $('#jumlah_obat').val(),
        "aturan_pakai": $('#aturan_pakai').val(),
        "cara_pakai": $('#cara_pakai').val(),
        "stabilitas_obat": $('#stabilitas_obat').val()
    };

    $.ajax({
        method: "POST",
        url: "update_farmasetis/" + id,
        contentType: "application/json",
        data: JSON.stringify(value)
    }).done(function(response) {
        alert("success")
        $('#save').attr('disabled', false);
        location.reload();
    })
}

function deleteData(id) {
    $.confirm({
        title: 'Delete data?',
        content: 'This dialog will automatically trigger \'cancel\' in 10 seconds if you don\'t respond.',
        autoClose: 'cancelAction|10000',
        buttons: {
            deleteAction: {
                text: 'delete',
                action: function() {
                    $.ajax({
                        method: "GET",
                        url: "delete_farmasetis/" + id,
                        contentType: "application/json"
                    }).done(function(response) {
                        $.alert('Deleted the user!');
                        location.reload();
                    })
                }
            },
            cancelAction: function() {
                $.alert('action is canceled');
            }
        }
    });

}

function showCombination(farmasetis_id) {
    $.ajax({
            method: "GET",
            url: "<?= base_url('findById') ?>",
            contentType: 'application/json; charset=utf-8',
            data: {
                farmasetis_id: farmasetis_id
            }
        })
        .done(function(msg) {
            console.log(msg)
            $('#list_obat').empty();
            let temp = JSON.parse(msg);
            let data = temp.kombinasi;
            for (let i = 0; i < data.length; i++) {
                let no = i + 1;
                let button = "";
                let action =
                    "<div class='btn btn-danger' onclick='deleteKombinasi(\"" +
                    temp.farmasetis.id + "\",\"" + data[i].farmasetis_y + "\")'>Delete</div>";
                if (data[i].interaksi_obat == 'Major') {
                    button = "<button class='btn' style='background-color:#ff0000; color:white'>Major</button>";
                } else if (data[i].interaksi_obat == 'Moderat') {
                    button = "<button class='btn' style='background-color:orange; color:white'>Moderat</button>";
                } else if (data[i].interaksi_obat == 'Minor') {
                    button = "<button class='btn' style='background-color:#2f96b4; color:white'>Minor</button>";
                } else if (data[i].interaksi_obat == 'Indikasi') {
                    button = "<button class='btn' style='background-color:yellow; color:black'>Indikasi</button>";
                }
                let list = '<tr>' +
                    '<th>' + no + '</th>' +
                    '<th>' + temp.farmasetis.nama_obat + '</th>' +
                    '<th>< ></th>' +
                    '<th>' + data[i].nama_obat + '</th>' +
                    '<th>' + button + '</th>' +
                    '<th>' + action + '</th>' +
                    '</tr>';
                $('#list_obat').append(list);
            }
            $('#title').text(temp.farmasetis.nama_obat)
            $('#modal-list').modal('show');
        });
}

function deleteKombinasi(farmsetis_x, farmasetis_y) {
    $.confirm({
        title: 'Delete data?',
        content: '',
        autoClose: 'cancel|10000',
        buttons: {
            deleteAction: {
                text: 'delete',
                action: function() {
                    $.ajax({
                        method: "GET",
                        url: "delete_kombinasi/" + farmsetis_x + "/" + farmasetis_y,
                        contentType: "application/json"
                    }).done(function(response) {
                        let data = JSON.parse(response);
                        if (data.code != "200") {
                            toastr.error(data.message);
                        } else {
                            toastr.success(data.message);
                            $('#modal-list').modal('hide');
                        }
                    })
                }
            },
            cancel: function() {
                $.alert('action is canceled');
            }
        }
    });

}
</script>