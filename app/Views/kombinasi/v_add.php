<div class="row">
    <div class="col-md-12">
        <!-- Advanced Tables -->

        <div class="panel panel-default">
            <div class="panel-heading">
            </div>
            <div class="panel-body">
                <form role="form" action="<?= base_url('add_kombinasi') ?>" method="post">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Pilih Obat</label>
                            <select class="form-control" id="kombinasi_list" onchange="getFarmasetisEx()">
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Kombinasi Obat :</label>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group" id="farmasetis_chekbox_list">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <!-- <div onclick="submit()" class="btn btn-primary">Submit</div> -->
                        <div id="save" onclick="submitMultipleCombination()" class="btn btn-primary">Submit</div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="<?php base_url() ?>template/assets/js/jquery-1.10.2.js"></script>
<!-- <script src="<?php base_url() ?>template/assets/js/bootstrap.min.js"></script> -->
<script>
$(document).ready(function() {
    getFarmasetis()
})
let farmasetisTemp = [];

function getFarmasetis() {
    $.ajax({
            method: "GET",
            url: "<?= base_url('findAllFarmasetis') ?>",
            contentType: "application/json",
        })
        .done(function(response) {
            console.log(response);
            console.log(typeof response)
            let result = JSON.parse(response);
            $('#kombinasi_list').empty();
            for (let i = 0; i < result.length; i++) {
                let option = '<option value="' + result[i].id + '">' + result[i].nama_obat + '</option>';
                $('#kombinasi_list').append(option);
            }
        })
}

function getFarmasetisEx() {
    let id = $('#kombinasi_list').val();
    $.ajax({
        method: "GET",
        url: "FarmasetisController/getAllFarmasetisEx",
        contentType: "application/json",
        data: {
            "id": id
        }
    }).done(function(response) {
        let resultTemp = JSON.parse(response);
        let result = resultTemp.farmasetis;
        let exKombinasi = resultTemp.kombinasi_obat;
        console.log();
        farmasetisTemp = result;
        $('#farmasetis_chekbox_list').empty();
        for (let i = 0; i < result.length; i++) {
            let checked = "";
            let value = "";
            for (let j = 0; j < exKombinasi.length; j++) {
                if (result[i].id == exKombinasi[j].farmasetis_y) {
                    checked = "checked";
                    value = exKombinasi[j].interaksi_obat;
                }
            }
            let div = '<div class="form-check" style="display:flex; margin-bottom:2%">\n\
                                    <label class="checkbox-inline" style="width: 130%;">\n\
                                        <input type="checkbox" class="checkbox_value" ' + checked +
                ' id="checkbox_value_' + i +
                '" value="' + result[i].id +
                '" />' + result[i].nama_obat + '\n\
                                    </label>\n\
                                    <select class="form-control" id="interaksi_obat_' + i + '">\n\
                                            <option value="Major">Major</option>\n\
                                            <option value="Moderat">Moderat</option>\n\
                                            <option value="Minor">Minor</option>\n\
                                            <option value="Indikasi">Indikasi</option>\n\
                                    </select>\n\
                                </div>';
            $('#farmasetis_chekbox_list').append(div)
            $('#interaksi_obat_' + i).val(value);
        }
    })

}

function submit() {
    let id_a = $('#kombinasi_list').val();
    let value = [];
    for (let i = 0; i < farmasetisTemp.length; i++) {
        if ($('#checkbox_value_' + i).is(":checked")) {
            let id_b = $('#checkbox_value_' + i).val();
            let interaksi = $('#interaksi_obat_' + i).val();
            let obj = {
                id: parseInt(id_b),
                interaksi_obat: interaksi
            }
            value.push(obj);
        }
    }

    $.ajax({
        method: "POST",
        url: "insertMultipleKombinasi",
        contentType: "json",
        data: JSON.stringify({
            "farmasetis_x": parseInt(id_a),
            "farmasetis_y": value
        })
    }).done(function(response) {
        window.location.href = "<?= base_url('kombinasi_list') ?>";
        // console.log(response)
    })
}

function submitMultipleCombination() {
    let id_a = $('#kombinasi_list').val();
    let value = [];
    for (let i = 0; i < farmasetisTemp.length; i++) {
        if ($('#checkbox_value_' + i).is(":checked")) {
            let id_b = $('#checkbox_value_' + i).val();
            let interaksi = $('#interaksi_obat_' + i).val();
            let obj = {
                id: parseInt(id_b),
                interaksi_obat: interaksi
            }
            value.push(obj);
        }
    }
    if ($('#kombinasi_list').val() == null || $('#kombinasi_list').val().trim() == "") {
        toastr.error("obat tidak boleh kosong");
        return false;
    }

    if (value.length < 1) {
        toastr.error("kombinasi obat belum dipilih");
        return false;
    }


    $.ajax({
        method: "POST",
        url: "insertMultipleKombinasi",
        contentType: "json",
        data: JSON.stringify({
            "farmasetis_x": parseInt(id_a),
            "farmasetis_y": value
        })
    }).done(function(response) {
        toastr.info("Berhasil dikombinasikan");
        $('#save').attr('disabled', true);
        setTimeout(function() {
            window.location.href = "<?= base_url('kombinasi_list') ?>";
        }, 2000)
    })
}
</script>