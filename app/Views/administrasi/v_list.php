<div class="row">
    <div class="col-md-12">
        <!-- Advanced Tables -->

        <div class="panel panel-default">
            <div class="panel-heading">

            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-8">
                        <button type="button" class="btn btn-primary" onclick="openform()">
                            Tambah Data
                        </button><br>
                    </div>
                </div>
                <br>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tbl_list">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Pasien</th>
                                <th>Tanggal Lahir</th>
                                <th>No Rekam Medis</th>
                                <th>Jenis Kelamin</th>
                                <th>No Telepon</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <!--End Advanced Tables -->
    </div>


    <div class="modal modal_1 fade" id="modal-xl">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Form Administrasi</h4>
                </div>
                <div class="modal-body custom_scroll">
                    <form id="form" role="form" action="<?= base_url('addadministrasi') ?>" method="post">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nama Pasien</label>
                                <input type="text" id="nama_pasien" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Tanggal Lahir</label>
                                <input type="date" id="tanggal_lahir" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">No. Rekam Medis</label>
                                <input type="number" id="no_rekam_medis" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Jenis Kelamin</label>
                                <select id="jenis_kelamin" class="form-control">
                                    <option value="laki-laki">Laki-laki</option>
                                    <option value="perempuan">Perempuan</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Berat Badan</label>
                                <input type="number" id="berat_badan" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">No. Telepon Pasien</label>
                                <input type="text" id="no_telepon_pasien" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Alamat Pasien</label>
                                <textarea id="alamat_pasien" class="form-control" placeholder=""></textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Nama Dokter</label>
                                <input type="text" id="nama_dokter" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Paraf dokter</label>
                                <input type="text" id="paraf_dokter" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">SIP dokter</label>
                                <input type="text" id="sip_dokter" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Tanggal Resep</label>
                                <input type="date" id="tanggal_resep" class="form-control" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Jenis Penjamin</label>
                                <select id="jenis_penjamin" class="form-control">
                                    <option value="Asuransi">Asuransi</option>
                                    <option value="BPJS">BPJS</option>
                                    <option value="Umum">Umum</option>
                                </select>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="modal-footer justify-content-between">
                    <div type="button" class="btn btn-default" data-dismiss="modal">Close</div>
                    <div type="button" class="btn btn-warning" onclick="reset()">Reset</div>
                    <div type="button" class="btn btn-primary" id="save" onclick="save()">Save</div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modal-ask">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" style="text-align: center; font-weight: 600">Apakah pasien sudah pernah
                        terdaftar?</h4>
                </div>
                <div class="modal-footer justify-content-between">
                    <div type="button" class="btn btn-default" onclick="closeForm('modal-ask')">TUTUP</div>
                    <div type="button" class="btn btn-warning" onclick="closeFormDirect('modal-ask')">TIDAK</div>
                    <a href="<?= base_url('klinis_list') ?>" class="btn btn-primary">&nbsp;&nbsp;&nbsp;&nbsp; YA
                        &nbsp;&nbsp;&nbsp;&nbsp;</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php base_url() ?>template/assets/js/jquery-1.10.2.js"></script>
<script>
$(document).ready(function() {
    let table = null;
    setTimeout(function() {
        table = $('#tbl_list').DataTable({
            "processing": true,
            "serverSide": true,
            "ordering": true, // Set true agar bisa di sorting
            "order": [
                [0, 'asc']
            ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
            "ajax": {
                "url": "<?= base_url('administrasi_datatable') ?>", // URL file untuk proses select datanya
                "type": "POST"
            },
            "deferRender": true,
            "aLengthMenu": [
                [10, 20, 50],
                [10, 20, 50]
            ], // Combobox Limit
            "columns": [{
                    "render": function(data, type, row) {
                        return "";
                    }
                }, {
                    "data": "nama_pasien"
                },
                {
                    "data": "tanggal_lahir"
                },
                {
                    "data": "no_rekam_medis"
                },
                {
                    "data": "jenis_kelamin"
                },
                {
                    "data": "no_telepon_pasien"
                },
                {
                    "render": function(data, type, row) { // Tampilkan kolom aksi
                        var html = '<button onclick="showData(\'' + row.id +
                            '\')" class="btn btn-primary">View or Edit</button>';
                        html += '&nbsp;&nbsp;&nbsp<button onclick="deleteData(\'' + row
                            .id +
                            '\')" class="btn btn-danger">Delete</button>';
                        return html
                    }
                },
            ],
        });

        table.on('order.dt search.dt', function() {
            let length = table.page.info().length;
            let currentPage = table.page.info().page + 1;
            let nomor = 1 + (length * (currentPage - 1));
            table.column(0, {
                search: 'applied',
                order: 'applied',
            }).nodes().each(function(cell, i) {
                let loop = i + 1;
                let num = nomor + loop - 1;
                cell.innerHTML = num;
            });
        }).draw();

        $('#modal-ask').modal('show');
    }, 1000)
});

function openform() {
    // $('#modal-xl').modal('show');
    $('#modal-xl').modal({
        'backdrop': 'static'
    });
    $('#save').attr('onclick', 'save()')
    reset()
}

function reset() {
    $('#form')[0].reset();
}

function save() {
    if (!validation()) {
        return;
    }
    $('#save').attr('disabled', true);
    let value = {
        "nama_pasien": $('#nama_pasien').val(),
        "tanggal_lahir": $('#tanggal_lahir').val(),
        "no_rekam_medis": $('#no_rekam_medis').val(),
        "jenis_kelamin": $('#jenis_kelamin').val(),
        "berat_badan": $('#berat_badan').val(),
        "no_telepon_pasien": $('#no_telepon_pasien').val(),
        "alamat_pasien": $('#alamat_pasien').val(),
        "nama_dokter": $('#nama_dokter').val(),
        "paraf_dokter": $('#paraf_dokter').val(),
        "sip_dokter": $('#sip_dokter').val(),
        "tanggal_resep": $('#tanggal_resep').val(),
        "jenis_penjamin": $('#jenis_penjamin').val()
    };

    $.ajax({
        method: "POST",
        url: "addadministrasi",
        contentType: "application/json",
        data: JSON.stringify(value)
    }).done(function(response) {
        toastr.info("Berhasil ditambahkan");
        $('#save').attr('disabled', false);
        setTimeout(function() {
            window.location.href = "<?= base_url('klinis_list') ?>";
        }, 2000)
    })
}

function showData(id) {
    $.ajax({
        method: "GET",
        url: "editadministrasi/" + id,
        contentType: "application/json",
    }).done(function(response) {
        let data = JSON.parse(response)
        console.log(data)
        // $('#modal-xl').modal('show');
        $('#modal-xl').modal({
            'backdrop': 'static'
        });
        $('#save').attr('onclick', 'update(' + data.id + ')')
        $('#nama_pasien').val(data.nama_pasien);
        $('#tanggal_lahir').val(data.tanggal_lahir);
        $('#no_rekam_medis').val(data.no_rekam_medis);
        $('#jenis_kelamin').val(data.jenis_kelamin);
        $('#berat_badan').val(data.berat_badan);
        $('#no_telepon_pasien').val(data.no_telepon_pasien);
        $('#alamat_pasien').val(data.alamat_pasien);
        $('#nama_dokter').val(data.nama_dokter);
        $('#paraf_dokter').val(data.paraf_dokter);
        $('#sip_dokter').val(data.sip_dokter);
        $('#tanggal_resep').val(data.tanggal_resep);
        $('#jenis_penjamin').val(data.jenis_penjamin);
    })
}

function update(id) {
    if (!validation()) {
        return;
    }
    $('#save').attr('disabled', true);
    let value = {
        "nama_pasien": $('#nama_pasien').val(),
        "tanggal_lahir": $('#tanggal_lahir').val(),
        "no_rekam_medis": $('#no_rekam_medis').val(),
        "jenis_kelamin": $('#jenis_kelamin').val(),
        "berat_badan": $('#berat_badan').val(),
        "no_telepon_pasien": $('#no_telepon_pasien').val(),
        "alamat_pasien": $('#alamat_pasien').val(),
        "nama_dokter": $('#nama_dokter').val(),
        "paraf_dokter": $('#paraf_dokter').val(),
        "sip_dokter": $('#sip_dokter').val(),
        "tanggal_resep": $('#tanggal_resep').val(),
        "jenis_penjamin": $('#jenis_penjamin').val()
    };

    $.ajax({
        method: "POST",
        url: "updateadministrasi/" + id,
        contentType: "application/json",
        data: JSON.stringify(value)
    }).done(function(response) {
        toastr.info("Berhasil diubah");
        $('#save').attr('disabled', false);
        setTimeout(function() {
            location.reload();
            // window.location.href = "<?= base_url('klinis_list') ?>";
        }, 2000)
    })
}

function validation() {
    if ($('#nama_pasien').val() == null || $('#nama_pasien').val().trim() == "") {
        toastr.error("nama pasien tidak boleh kosong");
        return false;
    }

    if ($('#tanggal_lahir').val() == null || $('#tanggal_lahir').val().trim() == "") {
        toastr.error("tanggal lahir tidak boleh kosong");
        return false;
    }

    if ($('#no_rekam_medis').val() == null) {
        toastr.error("nomor rekam medis tidak boleh kosong");
        return false;
    }

    if ($('#no_telepon_pasien').val() == null || $('#no_telepon_pasien').val().trim() == "") {
        toastr.error("nomor telepon pasien tidak boleh kosong");
        return false;
    }

    if ($('#nama_dokter').val() == null || $('#nama_dokter').val().trim() == "") {
        toastr.error("nama dokter tidak boleh kosong");
        return false;
    }

    if ($('#tanggal_resep').val() == null || $('#tanggal_resep').val().trim() == "") {
        toastr.error("tanggal resep tidak boleh kosong");
        return false;
    }

    if ($('#jenis_penjamin').val() == null || $('#jenis_penjamin').val().trim() == "") {
        toastr.error("penjamin tidak boleh kosong");
        return false;
    }

    return true;
}

function deleteData(id) {
    $.confirm({
        title: 'Delete data?',
        content: '',
        autoClose: 'cancelAction|10000',
        buttons: {
            deleteAction: {
                text: 'delete data',
                action: function() {
                    $.ajax({
                        method: "GET",
                        url: "deleteadministrasi/" + id,
                        contentType: "application/json"
                    }).done(function(response) {
                        let data = JSON.parse(response);
                        if (data.code != "200") {
                            toastr.error(data.message)
                        } else {
                            toastr.info("Data berhasil dihapus")
                        }
                    })
                }
            },
            cancelAction: function() {
                $.alert('dibatalkan');
            }
        }
    });
}

function closeForm(id) {
    $('#' + id).modal('hide');
}

function closeFormDirect(id) {
    $('#' + id).modal('hide');
    $('#modal-xl').modal({
        'backdrop': 'static'
    });
}
</script>