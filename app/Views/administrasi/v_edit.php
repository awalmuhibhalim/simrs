<div class="row">
    <div class="col-md-12">
        <!-- Advanced Tables -->

        <div class="panel panel-default">
            <div class="panel-heading">
            </div>
            <div class="panel-body">
                <form role="form" action="<?= base_url('updateadministrasi/' . $administrasi['id']); ?>" method="post">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nama Pasien</label>
                            <input type="text" name="nama_pasien" class="form-control" placeholder="Enter Username" value="<?= $administrasi['nama_pasien']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Tanggal Lahir</label>
                            <input type="date" name="tanggal_lahir" class="form-control" placeholder="" value="<?= $administrasi['tanggal_lahir']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">No. Rekam Medis</label>
                            <input type="number" name="no_rekam_medis" class="form-control" placeholder="" value="<?= $administrasi['no_rekam_medis']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Jenis Kelamin</label>
                            <select name="jenis_kelamin" class="form-control" value="<?= $administrasi['jenis_kelamin']; ?>">
                                <option value="laki-laki">Laki-laki</option>
                                <option value="perempuan">Perempuan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Berat Badan</label>
                            <input type="number" name="berat_badan" class="form-control" placeholder="" value="<?= $administrasi['berat_badan']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">No. Telepon Pasien</label>
                            <input type="text" name="no_telepon_pasien" class="form-control" placeholder="" value="<?= $administrasi['no_telepon_pasien']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Alamat Pasien</label>
                            <textarea type="password" name="alamat_pasien" class="form-control" placeholder=""><?= $administrasi['alamat_pasien']; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Nama Dokter</label>
                            <input type="text" name="nama_dokter" class="form-control" placeholder="" value="<?= $administrasi['nama_dokter']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Paraf dokter</label>
                            <input type="text" name="paraf_dokter" class="form-control" placeholder="" value="<?= $administrasi['paraf_dokter']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Tanggal Resep</label>
                            <input type="date" name="tanggal_resep" class="form-control" placeholder="" value="<?= $administrasi['tanggal_resep']; ?>">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Photo Resep</label>
                            <input type="file" name="photo_resep" class="form-control" placeholder="" value="<?= $administrasi['photo_resep']; ?>">
                        </div>

                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>

            </div>
        </div>
        <!--End Advanced Tables -->
    </div>
</div>