<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-8">
                        <div type="button" class="btn btn-primary" onclick="openform()">Tambah
                            Data</div>
                    </div>
                </div>
                <br>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tbl_userlist">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Username</th>
                                <th>Display Name</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modal-xl">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Form User</h4>
                </div>
                <div class="modal-body">
                    <form id="form" role="form" action="<?= base_url('adduser'); ?>" method="post">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Username</label>
                                <input type="text" id="username" class="form-control" placeholder="Enter Username">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="password" id="password" class="form-control" placeholder="Enter Password">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Display Name</label>
                                <input type="text" id="display_name" class="form-control"
                                    placeholder="Enter display name">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Role</label>
                                <select id="role" class="form-control">
                                    <option value="Staff">Staff</option>
                                    <option value="Kepala Instalasi">Kepala Instalasi</option>
                                </select>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="modal-footer justify-content-between">
                    <div type="button" class="btn btn-default" data-dismiss="modal">Close</div>
                    <div type="button" class="btn btn-warning" onclick="reset()">Reset</div>
                    <div type="button" class="btn btn-primary" id="save" onclick="save()">Save</div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<script src="<?php base_url() ?>template/assets/js/jquery-1.10.2.js"></script>
<script>
$(document).ready(function() {
    let table = null;
    setTimeout(function() {
        table = $('#tbl_userlist').DataTable({
            "processing": true,
            "serverSide": true,
            "ordering": true, // Set true agar bisa di sorting
            "order": [
                [0, 'asc']
            ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
            "ajax": {
                "url": "<?= base_url('user_datatable') ?>", // URL file untuk proses select datanya
                "type": "POST"
            },
            "deferRender": true,
            "aLengthMenu": [
                [10, 20, 50],
                [10, 20, 50]
            ], // Combobox Limit
            "columns": [{
                    "render": function(data, type, row) {
                        return "";
                    }
                }, {
                    "data": "username"
                },
                {
                    "data": "display_name"
                },
                {
                    "data": "role"
                },
                {
                    "render": function(data, type, row) { // Tampilkan kolom aksi
                        var html = '<button onclick="showData(\'' + row.id +
                            '\')" class="btn btn-primary">View or Edit</button>';
                        html += '&nbsp;&nbsp;&nbsp<button onclick="deleteData(\'' + row
                            .id +
                            '\')" class="btn btn-danger">Delete</button>';
                        return html
                    }
                },
            ],
        });

        table.on('order.dt search.dt', function() {
            let length = table.page.info().length;
            let currentPage = table.page.info().page + 1;
            let nomor = 1 + (length * (currentPage - 1));
            table.column(0, {
                search: 'applied',
                order: 'applied',
            }).nodes().each(function(cell, i) {
                let loop = i + 1;
                let num = nomor + loop - 1;
                cell.innerHTML = num;
            });
        }).draw();
    }, 1000)
});


function openform() {
    $('#modal-xl').modal('show');
    $('#save').attr('onclick', 'save()')
    reset()
}

function reset() {
    $('#form')[0].reset();
}

function save() {
    if (!validation()) {
        return;
    }

    $('#save').attr('disabled', true);
    let value = {
        "username": $('#username').val(),
        "password": $('#password').val(),
        "display_name": $('#display_name').val(),
        "role": $('#role').val(),
    };

    $.ajax({
        method: "POST",
        url: "adduser",
        contentType: "application/json",
        data: JSON.stringify(value)
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log(data);
        if (data.code != 200) {
            toastr.error(data.message)
            $('#save').attr('disabled', false);
        } else {
            toastr.info(data.message);
            setTimeout(function() {
                location.reload();
            }, 2000)
        }

    })
}

function showData(id) {
    $.ajax({
        method: "GET",
        url: "edituser/" + id,
        contentType: "application/json",
    }).done(function(response) {
        let data = JSON.parse(response)
        console.log(data)
        $('#modal-xl').modal('show');
        $('#save').attr('onclick', 'update(' + data.id + ')')
        $('#username').val(data.username);
        $('#password').val(data.password);
        $('#display_name').val(data.display_name);
        $('#role').val(data.role);
    })
}

function update(id) {
    if (!validation()) {
        return;
    }

    $('#save').attr('disabled', true);
    let value = {
        "username": $('#username').val(),
        "password": $('#password').val(),
        "display_name": $('#display_name').val(),
        "role": $('#role').val(),
    };

    $.ajax({
        method: "POST",
        url: "updateuser/" + id,
        contentType: "application/json",
        data: JSON.stringify(value)
    }).done(function(response) {
        let data = JSON.parse(response);
        console.log(data);
        if (data.code != 200) {
            toastr.error(data.message)
            $('#save').attr('disabled', false);
        } else {
            toastr.info(data.message);
            setTimeout(function() {
                location.reload();
            }, 2000)
        }
    })
}

function validation() {
    if ($('#username').val() == null || $('#username').val().trim() == "") {
        toastr.error("username tidak boleh kosong");
        return false;
    }

    if ($('#password').val() == null || $('#password').val().trim() == "") {
        toastr.error("password tidak boleh kosong");
        return false;
    }

    if ($('#username').val() == null || $('#username').val().trim() == "") {
        toastr.error("username tidak boleh kosong");
        return false;
    }

    if ($('#display_name').val() == null || $('#display_name').val().trim() == "") {
        toastr.error("nama tidak boleh kosong");
        return false;
    }

    return true;
}

function deleteData(id) {
    $.confirm({
        title: 'Delete data?',
        content: '',
        autoClose: 'cancel|10000',
        buttons: {
            deleteAction: {
                text: 'delete user',
                action: function() {
                    $.ajax({
                        method: "GET",
                        url: "deleteuser/" + id,
                        contentType: "application/json"
                    }).done(function(response) {
                        let data = JSON.parse(response);
                        if (data.code != "200") {
                            toastr.error(data.message);
                        } else {
                            toastr.info("User berhasil dihapus");
                            setTimeout(function() {
                                location.reload();
                            }, 2000)
                        }

                    })
                }
            },
            cancel: function() {
                $.alert('hapus data dibatalkan');
            }
        }
    });

}
</script>