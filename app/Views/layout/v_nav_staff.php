<!-- /. NAV TOP  -->
<nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
            <li class="text-center">
                <img src="<?php base_url() ?> template/assets/img/find_user.png" class="user-image img-responsive" />
                <h4 style="font-weight: 700; color: white;"><?= $_SESSION['display_name']; ?></h4>
            </li>


            <li>
                <a href="<?= base_url('index') ?>"><i class="fa fa-dashboard fa-2x"></i> Home</a>
            </li>
            <li>
                <a href="<?= base_url('administrasilist') ?>"><i class="fa fa-laptop fa-2x"></i> Administrasi</a>
            </li>
            <li>
                <a href="<?= base_url('klinis_list') ?>"><i class="fa fa-qrcode fa-2x"></i> Klinis</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-sitemap fa-2x"></i> Master<span class="fa arrow "></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?= base_url('report_klinis_list') ?>">Laporan</a>
                    </li>
                </ul>
            </li>
        </ul>

    </div>

</nav>
<!-- /. NAV SIDE  -->
<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h2><?= $title ?></h2>


            </div>
        </div>
        <!-- /. ROW  -->
        <hr />