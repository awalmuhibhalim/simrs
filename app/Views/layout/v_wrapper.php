<?php
if (!isset($_SESSION['username'])) {
    echo view('login/login');
} else {
    if ($_SESSION['role'] == "Staff") {
        echo view('layout/v_head');
        echo view('layout/v_header');
        echo view('layout/v_nav_staff');
        echo view('layout/v_content');
        echo view('layout/v_footer');
    } else {
        echo view('layout/v_head');
        echo view('layout/v_header');
        echo view('layout/v_nav');
        echo view('layout/v_content');
        echo view('layout/v_footer');
    }
}