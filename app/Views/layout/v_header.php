<?php
date_default_timezone_set('Asia/Jakarta');
$currentTime = date('d-m-Y H:i:s');
?>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">SIMRS</a>
            </div>
            <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;">
                <?php echo $currentTime; ?> &nbsp; <a href="<?= base_url('logout'); ?>"
                    class="btn btn-info square-btn-adjust">Logout</a>
                <input class="form-control" id="serach_global" placeholder="search..." />
            </div>
        </nav>
        <div class="modal fade" id="modal-info-obat">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Keterangan Obat</h4>
                    </div>
                    <div class="modal-body custom_scroll">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th width="200"></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id="info_detail_obat">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <div type="button" class="btn btn-default" data-dismiss="modal">Close</div>
                    </div>
                </div>
            </div>
        </div>