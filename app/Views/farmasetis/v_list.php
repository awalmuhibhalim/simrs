<div class="row">
    <div class="col-md-12">
        <!-- Advanced Tables -->

        <div class="panel panel-default">
            <div class="panel-heading">
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-8">
                        <div type="button" class="btn btn-primary" onclick="openform()">Tambah
                            Data</div>
                    </div>
                </div>
                <br>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tbl_list">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Obat</th>
                                <th>Bentuk Sediaan</th>
                                <th>Satuan</th>
                                <th>Jumlah</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <!--End Advanced Tables -->
    </div>


    <div class="modal fade" id="modal-xl">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Form Farmasetis</h4>
                </div>
                <div class="modal-body custom_scroll">
                    <form id="form" role="form" action="<?= base_url('add_farmasetis') ?>" method="post">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <h3>Farmasetis</h3>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nama Obat</label>
                                        <input type="text" id="nama_obat" class="form-control" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Bentuk Sediaan</label>
                                        <select class="form-control" id="bentuk_sediaan">
                                            <option value="Pulvis (serbuk)">Pulvis (serbuk)</option>
                                            <option value="Pulveres">Pulveres</option>
                                            <option value="Tablet (compressi)">Tablet (compressi)</option>
                                            <option value="Pilulae (PIL)">Pilulae (PIL)</option>
                                            <option value="Kapsul (capsule)">Kapsul (capsule)</option>
                                            <option value="Solutiones (Larutan)">Solutiones (Larutan)</option>
                                            <option value="Suspensi">Suspensi</option>
                                            <option value="Emulsi">Emulsi (elmusiones)</option>
                                            <option value="Infusa">Infusa</option>
                                            <option value="Imunoserum">Imunoserum (immunosera)</option>
                                            <option value="Salep">Salep (unguenta)</option>
                                            <option value="Suppositoria">Suppositoria</option>
                                            <option value="Obat tetes">Obat tetes (guttae)</option>
                                            <option value="Injeksi">Injeksi (injectiones)</option>
                                            <option value="Aerosol">Aerosol</option>
                                            <option value="Gel">Gel</option>
                                            <option value="Ovula">Ovula</option>
                                            <option value="Implan">Implan</option>
                                            <option value="Obat Kumur (Gargle)">Obat Kumur (Gargle)</option>
                                            <option value="Sirup">Sirup</option>
                                            <option value="Elixir">Elixir</option>
                                            <option value="potio">potio</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Kekuatan Sediaan</label>
                                        <input type="text" id="kekuatan_sediaan" class="form-control" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Satuan Sediaan</label>
                                        <input type="text" id="satuan_sediaan" class="form-control" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Jumlah Obat</label>
                                        <input type="number" id="jumlah_obat" class="form-control" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Aturan Pakai</label>
                                        <textarea id="aturan_pakai" class="form-control" placeholder=""></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Cara Pakai</label>
                                        <textarea id="cara_pakai" class="form-control" placeholder=""></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Stabilitas Obat</label>
                                        <input type="text" id="stabilitas_obat" class="form-control" placeholder="">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <h3>Klinis</h3>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Indikasi Obat</label>
                                        <input type="text" name="indikasi_obat" id="indikasi_obat" class="form-control"
                                            placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Dosis Obat</label>
                                        <input type="text" name="dosis_obat" id="dosis_obat" class="form-control"
                                            placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Rute Pemberian Obat</label>
                                        <input type="text" name="rute_pemberian_obat" id="rute_pemberian_obat"
                                            class="form-control" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Tepat Waktu</label>
                                        <input type="text" name="tepat_waktu" id="tepat_waktu" class="form-control"
                                            placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Duplikasi</label>
                                        <input type="text" name="duplikasi" id="duplikasi" class="form-control"
                                            placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Alergi</label>
                                        <textarea id="alergi" class="form-control" placeholder=""></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Interaksi Obat</label>
                                        <textarea name="interaksi_obat" id="interaksi_obat" class="form-control"
                                            placeholder=""></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Kontraindikasi Obat</label>
                                        <input type="text" name="kontraindikasi_obat" id="kontraindikasi_obat"
                                            class="form-control" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Efek Samping</label>
                                        <textarea id="efek_samping" class="form-control" placeholder=""></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer justify-content-between">
                    <div type="button" class="btn btn-default" data-dismiss="modal">Close</div>
                    <div type="button" class="btn btn-warning" onclick="reset()">Reset</div>
                    <div type="button" class="btn btn-primary" id="save" onclick="save()">Save</div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal-list">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Kombinasi <b><span id="title"></span></b></h4>
                </div>
                <div class="modal-body">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>Interaksi Obat</th>
                            </tr>
                        </thead>
                        <tbody id="list_obat">

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>
<script src="<?php base_url() ?>template/assets/js/jquery-1.10.2.js"></script>
<script>
$(document).ready(function() {
    let table = null;
    setTimeout(function() {
        table = $('#tbl_list').DataTable({
            "processing": true,
            "serverSide": true,
            "ordering": true, // Set true agar bisa di sorting
            "order": [
                [0, 'asc']
            ], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
            "ajax": {
                "url": "<?= base_url('farmasetis_datatable') ?>", // URL file untuk proses select datanya
                "type": "POST"
            },
            "deferRender": true,
            "aLengthMenu": [
                [10, 20, 50],
                [10, 20, 50]
            ], // Combobox Limit
            "columns": [{
                    "render": function(data, type, row) {
                        return "";
                    }
                }, {
                    "data": "nama_obat"
                },
                {
                    "data": "bentuk_sediaan"
                },
                {
                    "data": "satuan_sediaan"
                },
                {
                    "data": "jumlah_obat"
                },
                // {
                //     "render": function(data, type, row) { // Tampilkan jenis kelamin
                //         var html = ""
                //         if (row.jenis_kelamin == 1) { // Jika jenis kelaminnya 1
                //             html = 'Laki-laki' // Set laki-laki
                //         } else { // Jika bukan 1
                //             html = 'Perempuan' // Set perempuan
                //         }
                //         return html; // Tampilkan jenis kelaminnya
                //     }
                // },
                // {
                //     "data": "telp"
                // }, // Tampilkan telepon
                {
                    "render": function(data, type, row) { // Tampilkan kolom aksi
                        var html = '<button onclick="showData(\'' + row.id +
                            '\')" class="btn btn-primary">View or Edit</button>';
                        html += '&nbsp;&nbsp;&nbsp<button onclick="deleteData(\'' + row
                            .id +
                            '\')" class="btn btn-danger">Delete</button>';
                        return html
                    }
                },
            ],
        });

        table.on('order.dt search.dt', function() {
            let length = table.page.info().length;
            let currentPage = table.page.info().page + 1;
            let nomor = 1 + (length * (currentPage - 1));
            table.column(0, {
                search: 'applied',
                order: 'applied',
            }).nodes().each(function(cell, i) {
                let loop = i + 1;
                let num = nomor + loop - 1;
                cell.innerHTML = num;
            });
        }).draw();
    }, 1000)
});

function openform() {
    // $('#modal-xl').modal('show');
    $('#modal-xl').modal({
        'backdrop': 'static'
    });
    $('#save').attr('onclick', 'save()')
    reset()
}

function reset() {
    $('#form')[0].reset();
}

function save() {

    if (!validation()) {
        return;
    }

    $('#save').attr('disabled', true);
    let value = {
        "nama_obat": $('#nama_obat').val(),
        "bentuk_sediaan": $('#bentuk_sediaan').val(),
        "kekuatan_sediaan": $('#kekuatan_sediaan').val(),
        "satuan_sediaan": $('#satuan_sediaan').val(),
        "jumlah_obat": $('#jumlah_obat').val(),
        "aturan_pakai": $('#aturan_pakai').val(),
        "cara_pakai": $('#cara_pakai').val(),
        "stabilitas_obat": $('#stabilitas_obat').val(),
        "indikasi_obat": $('#indikasi_obat').val(),
        "dosis_obat": $('#dosis_obat').val(),
        "rute_pemberian_obat": $('#rute_pemberian_obat').val(),
        "tepat_waktu": $('#tepat_waktu').val(),
        "duplikasi": $('#duplikasi').val(),
        "alergi": $('#alergi').val(),
        "interaksi_obat": $('#interaksi_obat').val(),
        "kontraindikasi_obat": $('#kontraindikasi_obat').val(),
        "efek_samping": $('#efek_samping').val()
    };

    $.ajax({
        url: "add_farmasetis",
        type: "POST",
        dataType: "json",
        data: value
    }).done(function(response) {
        toastr.info("Berhasil ditambahkan");
        setTimeout(function() {
            location.reload();
        }, 2000)
    })
    setTimeout(function() {
        location.reload();
    }, 2000)
}

function showData(id) {
    $.ajax({
        method: "GET",
        url: "edit_farmasetis/" + id,
        contentType: "application/json",
    }).done(function(response) {
        let data = JSON.parse(response)
        console.log(data)
        // $('#modal-xl').modal('show');
        $('#modal-xl').modal({
            'backdrop': 'static'
        });
        $('#save').attr('onclick', 'update(' + data.id + ')')
        $('#nama_obat').val(data.nama_obat);
        $('#bentuk_sediaan').val(data.bentuk_sediaan);
        $('#kekuatan_sediaan').val(data.kekuatan_sediaan);
        $('#satuan_sediaan').val(data.satuan_sediaan);
        $('#jumlah_obat').val(data.jumlah_obat);
        $('#aturan_pakai').val(data.aturan_pakai);
        $('#cara_pakai').val(data.cara_pakai);
        $('#stabilitas_obat').val(data.stabilitas_obat);
        $('#indikasi_obat').val(data.indikasi_obat)
        $('#dosis_obat').val(data.dosis_obat);
        $('#rute_pemberian_obat').val(data.rute_pemberian_obat);
        $('#tepat_waktu').val(data.tepat_waktu);
        $('#duplikasi').val(data.duplikasi);
        $('#alergi').val(data.alergi);
        $('#interaksi_obat').val(data.interaksi_obat);
        $('#kontraindikasi_obat').val(data.kontraindikasi_obat);
        $('#efek_samping').val(data.efek_samping);
    })
}

function update(id) {

    if (!validation()) {
        return;
    }

    $('#save').attr('disabled', true);
    let value = {
        "nama_obat": $('#nama_obat').val(),
        "bentuk_sediaan": $('#bentuk_sediaan').val(),
        "kekuatan_sediaan": $('#kekuatan_sediaan').val(),
        "satuan_sediaan": $('#satuan_sediaan').val(),
        "jumlah_obat": $('#jumlah_obat').val(),
        "aturan_pakai": $('#aturan_pakai').val(),
        "cara_pakai": $('#cara_pakai').val(),
        "stabilitas_obat": $('#stabilitas_obat').val(),
        "indikasi_obat": $('#indikasi_obat').val(),
        "dosis_obat": $('#dosis_obat').val(),
        "rute_pemberian_obat": $('#rute_pemberian_obat').val(),
        "tepat_waktu": $('#tepat_waktu').val(),
        "duplikasi": $('#duplikasi').val(),
        "alergi": $('#alergi').val(),
        "interaksi_obat": $('#interaksi_obat').val(),
        "kontraindikasi_obat": $('#kontraindikasi_obat').val(),
        "efek_samping": $('#efek_samping').val()
    };

    $.ajax({
        method: "POST",
        url: "update_farmasetis/" + id,
        dataType: "application/json",
        data: value
    }).done(function(response) {
        toastr.info("Berhasil diubah");
        setTimeout(function() {
            location.reload();
        }, 2000)
    })

    setTimeout(function() {
        location.reload();
    }, 2000)
}

function validation() {

    if ($('#nama_obat').val() == null || $('#nama_obat').val().trim() == "") {
        toastr.error("nama obat tidak boleh kosong");
        return false;
    }

    if ($('#bentuk_sediaan').val() == null || $('#bentuk_sediaan').val().trim() == "") {
        toastr.error("bentuk sediaan tidak boleh kosong");
        return false;
    }

    return true;
}

function deleteData(id) {
    $.confirm({
        title: 'Delete data?',
        content: '',
        autoClose: 'cancel|10000',
        buttons: {
            deleteAction: {
                text: 'delete',
                action: function() {
                    $.ajax({
                        method: "GET",
                        url: "delete_farmasetis/" + id,
                        contentType: "application/json"
                    }).done(function(response) {
                        let data = JSON.parse(response);
                        if (data.code != "200") {
                            toastr.error(data.message);
                        } else {
                            toastr.info("Farmasetis berhasil dihapus");
                            setTimeout(function() {
                                location.reload();
                            }, 2000)
                        }
                    })
                }
            },
            cancel: function() {
                $.alert('hapus data dibatalkan');
            }
        }
    });

}

function showCombination(farmasetis_id) {
    $.ajax({
            method: "GET",
            url: "<?= base_url('findById') ?>",
            contentType: 'application/json; charset=utf-8',
            data: {
                farmasetis_id: farmasetis_id
            }
        })
        .done(function(msg) {
            console.log(msg)
            $('#list_obat').empty();
            let temp = JSON.parse(msg);
            let data = temp.kombinasi;
            for (let i = 0; i < data.length; i++) {
                let no = i + 1;
                let button = "";
                if (data[i].interaksi_obat == 'Major') {
                    button = "<button class='btn' style='background-color:#ff0000; color:white'>Major</button>";
                } else if (data[i].interaksi_obat == 'Moderat') {
                    button = "<button class='btn' style='background-color:#ff9999; color:white'>Moderat</button>";
                } else if (data[i].interaksi_obat == 'Minor') {
                    button = "<button class='btn' style='background-color:#ffe6e6; color:black'>Minor</button>";
                } else if (data[i].interaksi_obat == 'Indikasi') {
                    button = "<button class='btn' style='background-color:yellow; color:black'>Indikasi</button>";
                }
                let list = '<tr>' +
                    '<th>' + no + '</th>' +
                    '<th>' + temp.farmasetis.nama_obat + '</th>' +
                    '<th>< ></th>' +
                    '<th>' + data[i].nama_obat + '</th>' +
                    '<th>' + button + '</th>' +
                    '</tr>';
                $('#list_obat').append(list);
            }
            $('#title').text(temp.farmasetis.nama_obat)
            $('#modal-list').modal('show');
        });
}

function EditCombination() {
    $.ajax({
            method: "POST",
            url: "some.php",
            data: {
                name: "John",
                location: "Boston"
            }
        })
        .done(function(msg) {

            alert("Data Saved: " + msg);
        });
}
</script>