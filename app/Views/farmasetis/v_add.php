<div class="row">
    <div class="col-md-12">
        <!-- Advanced Tables -->

        <div class="panel panel-default">
            <div class="panel-heading">
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-5">
                        <form role="form" action="<?= base_url('add_farmasetis') ?>" method="post">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Obat</label>
                                    <input type="text" name="nama_obat" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Bentuk Sediaan</label>
                                    <input type="text" name="bentuk_sediaan" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Kekuatan Sediaan</label>
                                    <input type="text" name="kekuatan_sediaan" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Satuan Sediaan</label>
                                    <input type="text" name="satuan_sediaan" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Jumlah Obat</label>
                                    <input type="number" name="jumlah_obat" class="form-control" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Aturan Pakai</label>
                                    <textarea name="aturan_pakai" class="form-control" placeholder=""></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Cara Pakai</label>
                                    <textarea name="cara_pakai" class="form-control" placeholder=""></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Stabilitas Obat</label>
                                    <input type="text" name="stabilitas_obat" class="form-control" placeholder="">
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
        <!--End Advanced Tables -->
    </div>
</div>