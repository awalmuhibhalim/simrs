<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;
use App\Models\HistoryModel;

date_default_timezone_set('Asia/Jakarta');
class AdministrasiModel extends Model
{
    protected $table = "administrasi";
    protected $useTimesTimps = true;
    protected $allowedFields = [
        'nama_pasien',
        'tanggal_lahir',
        'no_rekam_medis',
        'jenis_kelamin',
        'berat_badan',
        'no_telepon_pasien',
        'alamat_pasien',
        'nama_dokter',
        'sip_dokter',
        'paraf_dokter',
        'tanggal_resep',
        'photo_resep'
    ];

    public function filter($search, $limit, $start, $order_field, $order_ascdesc)
    {
        return $this->table('administrasi')->like('nama_pasien', $search)
            ->orLike('no_rekam_medis', $search)
            ->orLike('nama_dokter', $search)
            ->orLike('no_telepon_pasien', $search)
            // ->order_by($order_field, $order_ascdesc)
            ->limit($limit, $start)
            ->get()->getResultArray();
    }
    public function count_filter($search)
    {
        return $this->table('administrasi')->like('nama_pasien', $search)
            ->orLike('no_rekam_medis', $search)
            ->orLike('nama_dokter', $search)
            ->orLike('no_telepon_pasien', $search)
            ->countAllResults();
    }

    public function getList()
    {
        try {
            return $this->table('administrasi');
        } catch (Exception $ex) {
        }
    }

    public function getAdminList()
    {
        try {
            return $this->db->table('administrasi')->get()->getResultArray();
        } catch (Exception $ex) {
        }
    }

    public function insertAdministrasi($data)
    {
        try {
            $result = $this->db->table('administrasi')->insert($data);
            $history = [
                'activity' => 'Menambahkan Data Administrasi',
                'created_at' => date('d-m-Y H:i:s'),
                'creator' => $_SESSION['username']
            ];

            $objHistory = new HistoryModel();
            $objHistory->insertHistory($history);
            return $result;
        } catch (Exception $ex) {
        }
    }

    public function editAdministrasi($id)
    {
        return $this->db->table('administrasi')->where('id', $id)->get()->getRowArray();
    }

    public function updateAdministrasi($data, $id)
    {
        try {
            $result = $this->db->table('administrasi')->update($data, array('id' => $id));
            $history = [
                'activity' => 'Mengubah Data Administrasi',
                'created_at' => date('d-m-Y H:i:s'),
                'creator' => $_SESSION['username']
            ];

            $objHistory = new HistoryModel();
            $objHistory->insertHistory($history);
            return $result;
        } catch (Exception $ex) {
        }
    }

    public function deleteAdministrasi($id)
    {
        try {
            $row = $this->db->query("select * from klinis where administrasi_id = $id");
            if (isset($row)) {
                return false;
            } else {
                $result = $this->db->table('administrasi')->delete(array('id' => $id));
                $history = [
                    'activity' => 'Menghapus Data Administrasi',
                    'created_at' => date('d-m-Y H:i:s'),
                    'creator' => $_SESSION['username']
                ];

                $objHistory = new HistoryModel();
                $objHistory->insertHistory($history);
                return true;
            }
        } catch (Exception $ex) {
        }
    }

    public function search($keyword)
    {
        return $this->table('administrasi')->like('nama_pasien', $keyword)->orLike('no_rekam_medis', $keyword);
    }

    public function searchAutoComplete($keyword)
    {
        return $this->db->table('administrasi')
            ->like('nama_pasien', $keyword)
            ->orLike('no_rekam_medis', $keyword)
            ->orLike('nama_dokter', $keyword)
            ->get()
            ->getResultArray();
    }
}