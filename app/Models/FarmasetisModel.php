<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;
use App\Models\HistoryModel;

date_default_timezone_set('Asia/Jakarta');

class FarmasetisModel extends Model
{
    protected $table = 'farmasetis';
    protected $useTimesTamps = true;
    protected $allowedFields = [
        "nama_obat",
        "bentuk_sediaan",
        "kekuatan_sediaan",
        "satuan_sediaan",
        "jumlah_obat",
        "aturan_pakai",
        "cara_pakai",
        "stabilitas_obat"
    ];

    public function filter($search, $limit, $start, $order_field, $order_ascdesc)
    {
        return $this->table('farmasetis')->like('nama_obat', $search)
            ->orLike('bentuk_sediaan', $search)
            ->orLike('kekuatan_sediaan', $search)
            ->orLike('aturan_pakai', $search)
            // ->order_by($order_field, $order_ascdesc)
            ->limit($limit, $start)
            ->get()->getResultArray();
    }
    public function count_filter($search)
    {
        return $this->table('farmasetis')->like('nama_obat', $search)
            ->orLike('bentuk_sediaan', $search)
            ->orLike('kekuatan_sediaan', $search)
            ->orLike('aturan_pakai', $search)
            ->countAllResults();
    }

    public function getFarmasetisList()
    {
        return $this->table('farmasetis');
    }

    public function findById($id)
    {
        try {
            return $this->db->table('farmasetis')->where('id', $id)
                ->get()
                ->getRowArray();
        } catch (Exception $ex) {
        }
    }

    public function insertFarmasetis($data)
    {
        try {
            $result = $this->db->table('farmasetis')->insert($data);
            $history = [
                'activity' => 'Menambahkan Data Farmasetis',
                'created_at' => date('d-m-Y H:i:s'),
                'creator' => $_SESSION['username']
            ];

            $objHistory = new HistoryModel();
            $objHistory->insertHistory($history);
            return $result;
        } catch (Exception $ex) {
        }
    }

    public function updateFarmasetis($data, $id)
    {
        try {
            $result = $this->db->table('farmasetis')->update($data, array('id' => $id));
            $history = [
                'activity' => 'Mengubah Data Farmasetis',
                'created_at' => date('d-m-Y H:i:s'),
                'creator' => $_SESSION['username']
            ];

            $objHistory = new HistoryModel();
            $objHistory->insertHistory($history);
            return $result;
        } catch (Exception $ex) {
        }
    }

    public function deleteFarmasetis($id)
    {
        try {
            $row = $this->db->query("select * from kombinasi_obat where farmasetis_x=$id or farmasetis_y=$id")->getRow();
            if (isset($row)) {
                return false;
            } else {
                $result = $this->db->table('farmasetis')->delete(array('id' => $id));
                $history = [
                    'activity' => 'Menghapus Data Farmasetis',
                    'created_at' => date('d-m-Y H:i:s'),
                    'creator' => $_SESSION['username']
                ];

                $objHistory = new HistoryModel();
                $objHistory->insertHistory($history);
                return true;
            }
        } catch (Exception $ex) {
        }
    }

    public function search($keyword)
    {
        try {
            return $this->table('farmasetis')->like('nama_obat', $keyword)->orLike('aturan_pakai', $keyword);
        } catch (Exception $ex) {
        }
    }

    public function retrieveAllFarmasetis()
    {
        $reult = $this->db->table('farmasetis')->get()->getResultArray();

        return $reult;
    }

    public function getAllFarmasetisEx($id)
    {
        $result = $this->db->query("select id, nama_obat from farmasetis where id != $id")->getResultArray();
        return $result;
    }

    public function searchFarmasetis($keyword)
    {
        $reult = $this->db->table('farmasetis')->like('nama_obat', $keyword)->orLike('aturan_pakai', $keyword)->get()->getResultArray();

        return $reult;
    }


    function getFarmasetisAutoComplete($postData)
    {
        $response = array();
        if (isset($postData)) {
            $records = $this->db->table('farmasetis')->select('*')->where("nama_obat like '%" . $postData . "%' ")->get()->getResultArray();
            for ($i = 0; $i < count($records); $i++) {
                $response[] = array("value" => $records[$i]['id'], "label" => $records[$i]['nama_obat']);
            }
        }
        return $response;
    }
}