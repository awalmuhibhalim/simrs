<?php

namespace App\Models;

use CodeIgniter\Model;

class HistoryModel extends Model
{
    protected $table = 'history';

    public function filter($search, $limit, $start, $order_field, $order_ascdesc)
    {

        $result = $this->table('history')->like('creator', $search)
            ->orLike('activity', $search)
            ->orLike('created_at', $search)
            // ->order_by('id', 'DESC')
            ->limit($limit, $start)
            ->get()->getResultArray();
        // dd($result);
        return $result;
    }

    public function count_filter($search)
    {
        $result =  $this->table('history')->like('creator', $search)
            ->orLike('activity', $search)
            ->orLike('created_at', $search)
            ->countAllResults();
        return $result;
    }

    public function insertHistory($data)
    {
        return $this->db->table("history")->insert($data);
    }
}