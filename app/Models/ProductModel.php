<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;

class ProductModel extends Model {

    public function getProduct($id = false) {
        if ($id === false) {
            return $this->db->table('product')
                            ->get()
                            ->getResultArray();
        } else {
            return $this->db->table('product')
                            ->where('product_id', $id)
                            ->get()
                            ->getRowArray();
        }
    }

    public function getAllProduct() {
        try {
            return $this->db->table('product')
                            ->get()
                            ->getResultArray();
            
            die($this->db->getLastQuery());
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

}
