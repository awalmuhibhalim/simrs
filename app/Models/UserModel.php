<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;
use App\Models\HistoryModel;

date_default_timezone_set('Asia/Jakarta');
class UserModel extends Model
{
    protected $table = 'user';
    protected $useTimestamps = true;
    protected $allowedFields = ['username', 'password', 'display_name'];

    public function filter($search, $limit, $start, $order_field, $order_ascdesc)
    {
        return $this->table('user')->like('username', $search)
            ->orLike('display_name', $search)
            ->orLike('role', $search)
            // ->order_by($order_field, $order_ascdesc)
            ->limit((int) $limit, (int) $start)
            ->get()->getResultArray();
    }
    public function count_filter($search)
    {
        return $this->table('user')->like('username', $search)
            ->orLike('display_name', $search)
            ->orLike('role', $search)
            ->countAllResults();
    }

    public function getUserList()
    {
        try {
            return $this->table('user');
        } catch (Exception $ex) {
        }
    }

    public function insertUser($data)
    {
        $result = $this->db->table('user')->insert($data);
        $history = [
            'activity' => 'Menambahkan Data User',
            'created_at' => date('d-m-Y H:i:s'),
            'creator' => $_SESSION['username']
        ];

        $objHistory = new HistoryModel();
        $objHistory->insertHistory($history);
        return $result;
    }

    public function editUser($id)
    {
        return $this->db->table('user')->where('id', $id)
            ->get()
            ->getRowArray();
    }

    public function updateUser($data, $id)
    {
        $result = $this->db->table('user')->update($data, array('id' => $id));
        $history = [
            'activity' => 'Mengubah Data User',
            'created_at' => date('d-m-Y H:i:s'),
            'creator' => $_SESSION['username']
        ];

        $objHistory = new HistoryModel();
        $objHistory->insertHistory($history);
        return $result;
    }

    public function deleteUser($id)
    {
        $row = $this->db->query("select * from user where id=$id")->getRow();
        if (isset($row)) {
            if ($row->username == $_SESSION['username'] && $row->role == "Manager") {
                return false;
            } else {
                $this->db->table('user')->delete(array('id' => $id));
                $history = [
                    'activity' => 'Menghapus Data User ' . $row->username . '(' . $row->role . ')',
                    'created_at' => date('d-m-Y H:i:s'),
                    'creator' => $_SESSION['username']
                ];

                $objHistory = new HistoryModel();
                $objHistory->insertHistory($history);
                return true;
            }
        }
    }

    public function keyword($keyword)
    {
        return $this->table('user')->like('username', $keyword)->orLike('display_name', $keyword);
    }

    public function proccessLogin($username, $password)
    {
        $row = $this->db->query("SELECT * FROM user WHERE username='$username' AND password='$password'")->getRow();
        if (isset($row)) {
            $_SESSION['username'] = $row->username;
            $_SESSION['password'] = $row->password;
            $_SESSION['display_name'] = $row->display_name;
            $_SESSION['role'] = $row->role;
            if (isset($_SESSION['display_name'])) {
                // var_dump("Login Success ini display name " . $_SESSION['display_name']);
            }
            $history = [
                'activity' => 'Melakukan Login',
                'created_at' => date('d-m-Y H:i:s'),
                'creator' => $_SESSION['username']
            ];

            $objHistory = new HistoryModel();
            $objHistory->insertHistory($history);
            return true;
            // var_dump("Login Success");
            // var_dump($row->display_name);
        } else {
            return false;
            // var_dump("Login Failed");
        }
    }

    public function findByUsername($id = null, $username)
    {
        $result = false;
        if (isset($id)) {
            $row = $this->db->query("SELECT * FROM user WHERE id=$id")->getRow();
            if (isset($row)) {
                $rows = $this->db->query("SELECT * FROM user WHERE username='$username'")->getRow();
                if (isset($rows)) {
                    if ($rows->username == $username) {
                        $result = true;
                    } else {
                        $result = false;
                    }
                } else {
                    $result = false;
                }
            } else {
                $result = false;
            }
        } else {
            $row = $this->db->query("SELECT * FROM user WHERE username='$username'")->getRow();
            if (isset($row)) {
                $result = true;
            } else {
                $result = false;
            }
        }

        return $result;
    }

    public function selectCount()
    {
        $result = $this->db->query("select * from user where role='manager'")->countAllResults();
        return $result;
    }
}