<?php

namespace App\Models;

use CodeIgniter\Model;
use CodeIgniter\Modules;

class AboutModel extends Model
{
    protected $table = 'about';
    public function insetAbout($data)
    {
        $this->db->table('about')->insert($data);
    }
}