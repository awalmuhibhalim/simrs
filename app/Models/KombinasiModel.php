<?php

namespace App\Models;

use CodeIgniter\Model;
use Exception;
use CodeIgniter\Database\Query;
use App\Models\HistoryModel;

date_default_timezone_set('Asia/Jakarta');

class KombinasiModel extends Model
{
    // protected $kombinasiModel;
    protected $table = 'kombinasi_obat';
    protected $useTimesTamps = true;
    protected $allowedFields = [
        "farmasetis_x",
        "farmasetis_y",
        "interaksi_obat"
    ];

    // public function __construct()
    // {
    //     $this->kombinasiModel = new KombinasiModel();
    // }

    public function getKombinasiList()
    {
        return $this->table('kombinasi_obat');
    }

    public function selectAllCopy()
    {
        // $sql = "SELECT * FROM some_table WHERE id = ? AND status = ? AND author = ?";
        // return $db->query($sql, [3, 'live', 'Rick']);
        // $sql = "SELECT a.*, b.* FROM farmasetis a, kombinasi_obat b WHERE a.id = b.farmasetis_x or a.id = b.farmasetis_y";
        // return $this->db->table('kombinasi_obat')->query($sql)->get()->getResultArray();

    }

    public function selectAll()
    {
        return $this->table('kombinasi_obat')->join('farmasetis', 'farmasetis.id = kombinasi_obat.farmasetis_x');
    }

    public function detailKombinasi($farmasetis_x)
    {
        $data = $this->db->table('kombinasi_obat')->select("kombinasi_obat.farmasetis_x as farmasetis_x,kombinasi_obat.farmasetis_y as farmasetis_y, kombinasi_obat.interaksi_obat as interaksi_obat, farmasetis.nama_obat as nama_obat")->join('farmasetis', "farmasetis.id = kombinasi_obat.farmasetis_y and kombinasi_obat.farmasetis_x = $farmasetis_x")->get()->getResultArray();
        return $data;
    }

    public function findById($id)
    {
        try {
            return $this->db->table('kombinasi_obat')->where('id', $id)
                ->get()
                ->getRowArray();
        } catch (Exception $ex) {
        }
    }

    public function insertKombinasi($data)
    {
        try {
            $this->db->table('kombinasi_obat')->insert($data);
        } catch (Exception $ex) {
        }
    }

    public function updateKombinasi($data, $id)
    {
        try {
            return $this->db->table('kombinasi_obat')->update($data, array('id' => $id));
        } catch (Exception $ex) {
        }
    }

    public function updateKombinasiInteraksi($data, $farmasetis_x, $farmasetis_y)
    {
        try {
            $result = $this->db->table('kombinasi_obat')->update($data, array('farmasetis_x' => (int) $farmasetis_x, 'farmasetis_y' => $farmasetis_y));
            return $result; // result = true or false
        } catch (Exception $ex) {
        }
    }
    public function deleteKombinasi($farmasetis_x, $farmasetis_y)
    {
        try {
            $row = $this->db->query("select * from kombinasi_obat where farmasetis_x=$farmasetis_x and farmasetis_y=$farmasetis_y")->getRow();
            if (isset($row)) {
                $this->db->table('kombinasi_obat')->delete(array('farmasetis_x' => $farmasetis_x, 'farmasetis_y' => $farmasetis_y));
                return true;
            } else {
                return false;
            }
        } catch (Exception $ex) {
        }
    }

    public function search($keyword)
    {
        try {
            return $this->table('kombinasi_obat')->like('farmasetis_x', $keyword)->orLike('farmasetis_y', $keyword);
        } catch (Exception $ex) {
        }
    }

    public function dragCompare($obat_a, $obat_b)
    {
        try {
            return $this->db->query("select * from kombinasi_obat where farmasetis_x= $obat_a and farmasetis_y=$obat_b")->getRowArray();
        } catch (Exception $ex) {
        }
    }

    public function insertMultiple($data)
    {
        try {
            $id = json_encode($data->farmasetis_x);
            $temp = json_encode($data->farmasetis_y);

            $tempA = json_decode($temp, true);
            for ($i = 0; $i < count($tempA); $i++) {
                var_dump($tempA[$i]['interaksi_obat']);
                var_dump($tempA[$i]['id']);
                $result = $this->validation($id, $tempA[$i]['id']);
                if ($result == null) {
                    $data = [
                        "farmasetis_x" => $id,
                        "farmasetis_y" => $tempA[$i]['id'],
                        "interaksi_obat" => $tempA[$i]['interaksi_obat']
                    ];
                    $this->insertKombinasi($data);
                } else {
                    $data = [
                        "interaksi_obat" => $tempA[$i]['interaksi_obat']
                    ];
                    $this->updateKombinasiInteraksi($data, $id, $tempA[$i]['id']);
                }
            }

            $history = [
                'activity' => 'Menambahkan/Mengubah Data Kombinasi obat',
                'created_at' => date('d-m-Y H:i:s'),
                'creator' => $_SESSION['username']
            ];

            $objHistory = new HistoryModel();
            $objHistory->insertHistory($history);

            return "success";
        } catch (Exception $ex) {
        }
    }

    public function validation($farmasetis_x, $farmasetis_y)
    {
        $data = $this->db->query("select * from kombinasi_obat where farmasetis_x= $farmasetis_x and farmasetis_y=$farmasetis_y")->getRowArray();
        var_dump($data);
        return $data;
    }

    public function insertQuery($farmasetis_x, $farmasetis_y, $interaksi_obat)
    {
        $sql = "INSERT INTO kombinasi_obat(farmasetis_x, farmasetis_y, interkasi_obat) VALUES ($farmasetis_x,$farmasetis_y, '$interaksi_obat')";

        return (new Query($db))->setQuery($sql);
    }
}