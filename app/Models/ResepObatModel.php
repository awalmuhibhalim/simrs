<?php

namespace App\Models;

use CodeIgniter\Model;

class ResepObatModel extends Model
{
    protected $table = 'resep_obat';
    protected $useTimesTamps = true;

    public function insertResepObat($data)
    {
        $this->db->table("resep_obat")->insert($data);
    }

    public function updateResepObat($data, $id)
    {
        $this->db->table("resep_obat")->update($data)->where($data, array('klinis_id' => $id));
    }

    public function deleteResepObat($klinis_id)
    {
        $this->db->table("resep_obat")->delete(array('klinis_id' => $klinis_id));
    }

    // public function findByKlinisId($klinis_id)
    // {
    //     $result = $this->db->table('resep_obat')->select("resep_obat.id as id, resep_obat.klinis_id as klinis_id, resep_obat.farmasetis_id as farmasetis_id, resep_obat.bentuk_sediaan as bentuk_sediaan, resep_obat.aturan_pakai as aturan_pakai, resep_obat.jumlah_obat as jumlah_obat, 
    //      farmasetis.nama_obat as nama_obat")
    //         ->join('farmasetis', 'resep_obat.farmasetis_id = farmasetis.id')
    //         ->where('klinis_id', $klinis_id)
    //         ->get()
    //         ->getResultArray();
    //     return $result;
    // }

    public function findByKlinisId($klinis_id)
    {
        $result = $this->db->table('resep_obat')->select("resep_obat.*, 
         farmasetis.nama_obat as nama_obat")
            ->join('farmasetis', 'resep_obat.farmasetis_id = farmasetis.id')
            ->where('resep_obat.klinis_id', $klinis_id)
            ->get()
            ->getResultArray();
        return $result;
    }
}