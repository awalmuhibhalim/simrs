<?php

namespace App\Controllers;

use App\Models\AdministrasiModel;

class AdministrasiController extends BaseController
{

    protected $admModel;

    public function __construct()
    {
        $this->admModel = new AdministrasiModel();
    }

    public function index()
    {
        $currentPage = $this->request->getVar('page') ? $this->request->getVar('page') : 1;
        $keyword = $this->request->getVar('keyword');
        if ($keyword) {
            $model = $this->admModel->search($keyword);
        } else {
            $model = $this->admModel;
        }

        $data = [
            'title' => 'Daftar Adiminstrasi',
            // 'administrasi' => $model->paginate(5),
            // 'pager' => $this->admModel->pager,
            // 'currentPage' => $currentPage,
            // 'total' => $this->admModel->countAllResults(),
            'isi' => 'administrasi/v_list'
        ];

        echo view('layout/v_wrapper', $data);
    }

    public function view()
    {

        $search = $_POST['search']['value']; // Ambil data yang di ketik user pada textbox pencarian
        $limit = $_POST['length']; // Ambil data limit per page
        $start = $_POST['start']; // Ambil data start
        $order_index = $_POST['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
        $order_field = $_POST['columns'][$order_index]['data']; // Untuk mengambil nama field yg menjadi acuan untuk sorting
        $order_ascdesc = $_POST['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"
        $sql_total = $this->admModel->countAllResults(); // Panggil fungsi count_all pada SiswaModel
        $sql_data = $this->admModel->filter($search, $limit, $start, $order_field, $order_ascdesc); // Panggil fungsi filter pada SiswaModel
        $sql_filter = $this->admModel->count_filter($search); // Panggil fungsi count_filter pada SiswaModel
        $callback = array(
            'draw' => $_POST['draw'], // Ini dari datatablenya
            'recordsTotal' => $sql_total,
            'recordsFiltered' => $sql_filter,
            'data' => $sql_data
        );
        header('Content-Type: application/json');
        echo json_encode($callback); // Convert array $callback ke json
    }

    public function openForm()
    {
        $data = [
            'title' => 'Tambah',
            'isi' => 'administrasi/v_add'
        ];

        echo view('layout/v_wrapper', $data);
    }

    public function openFormEdit($id)
    {
        return json_encode($this->admModel->editAdministrasi($id));
    }

    public function save()
    {
        if ($this->request->isAJAX()) {
            $temp = $this->request->getJSON();
            $nama_pasien = str_replace('"', '', json_encode($temp->nama_pasien));
            $tanggal_lahir = str_replace('"', '', json_encode($temp->tanggal_lahir));
            $no_rekam_medis = str_replace('"', '', json_encode($temp->no_rekam_medis));
            $jenis_kelamin = str_replace('"', '', json_encode($temp->jenis_kelamin));
            $berat_badan = str_replace('"', '', json_encode($temp->berat_badan));
            $no_telepon_pasien = str_replace('"', '', json_encode($temp->no_telepon_pasien));
            $alamat_pasien = str_replace('"', '', json_encode($temp->alamat_pasien));
            $nama_dokter = str_replace('"', '', json_encode($temp->nama_dokter));
            $sip_dokter = str_replace('"', '', json_encode($temp->sip_dokter));
            $paraf_dokter = str_replace('"', '', json_encode($temp->paraf_dokter));
            $tanggal_resep = str_replace('"', '', json_encode($temp->tanggal_resep));
            $jenis_penjamin = str_replace('"', '', json_encode($temp->jenis_penjamin));

            $data = [
                'nama_pasien' => $nama_pasien,
                'tanggal_lahir' => $tanggal_lahir,
                'no_rekam_medis' => $no_rekam_medis,
                'jenis_kelamin' => $jenis_kelamin,
                'berat_badan' => $berat_badan,
                'no_telepon_pasien' => $no_telepon_pasien,
                'alamat_pasien' => $alamat_pasien,
                'nama_dokter' => $nama_dokter,
                'sip_dokter' => $sip_dokter,
                'paraf_dokter' => $paraf_dokter,
                'tanggal_resep' => $tanggal_resep,
                'jenis_penjamin' => $jenis_penjamin,
                'created_at' => date('d-M-Y h:t:s'),
                'creator' => $_SESSION['username']
            ];

            $this->admModel->insertAdministrasi($data);
        }

        // session()->setFlashdata('success', 'data berhasil ditambahkan');
        // return redirect()->to(base_url('administrasilist'));
    }

    public function update($id)
    {
        if ($this->request->isAJAX()) {
            $temp = $this->request->getJSON();
            $nama_pasien = str_replace('"', '', json_encode($temp->nama_pasien));
            $tanggal_lahir = str_replace('"', '', json_encode($temp->tanggal_lahir));
            $no_rekam_medis = str_replace('"', '', json_encode($temp->no_rekam_medis));
            $jenis_kelamin = str_replace('"', '', json_encode($temp->jenis_kelamin));
            $berat_badan = str_replace('"', '', json_encode($temp->berat_badan));
            $no_telepon_pasien = str_replace('"', '', json_encode($temp->no_telepon_pasien));
            $alamat_pasien = str_replace('"', '', json_encode($temp->alamat_pasien));
            $nama_dokter = str_replace('"', '', json_encode($temp->nama_dokter));
            $sip_dokter = str_replace('"', '', json_encode($temp->sip_dokter));
            $paraf_dokter = str_replace('"', '', json_encode($temp->paraf_dokter));
            $tanggal_resep = str_replace('"', '', json_encode($temp->tanggal_resep));
            $jenis_penjamin = str_replace('"', '', json_encode($temp->jenis_penjamin));

            $data = [
                'nama_pasien' => $nama_pasien,
                'tanggal_lahir' => $tanggal_lahir,
                'no_rekam_medis' => $no_rekam_medis,
                'jenis_kelamin' => $jenis_kelamin,
                'berat_badan' => $berat_badan,
                'no_telepon_pasien' => $no_telepon_pasien,
                'alamat_pasien' => $alamat_pasien,
                'nama_dokter' => $nama_dokter,
                'sip_dokter' => $sip_dokter,
                'paraf_dokter' => $paraf_dokter,
                'tanggal_resep' => $tanggal_resep,
                'jenis_penjamin' => $jenis_penjamin,
                'updated_at' => date('d-M-Y h:t:s'),
                'creator' => $_SESSION['username']
            ];

            $this->admModel->updateAdministrasi($data, $id);
        }

        // $this->admModel->updateAdministrasi($data, $id);
        // session()->setFlashdata('success', 'data berhasil diupdate');
        // return redirect()->to(base_url('administrasilist'));
    }

    public function delete($id)
    {
        $response = [];
        $result = $this->admModel->deleteAdministrasi($id);
        if ($result) {
            $response = [
                "code" => 200,
                "message" => "Success"
            ];
        } else {
            $response = [
                "code" => "00",
                "message" => "Data Administrasi sudah digunakan di data klinis"
            ];
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function showAdministrasionList()
    {
        $keyword = $this->request->getVar('keyword');
        return json_encode($this->admModel->searchAutoComplete($keyword));
    }
}