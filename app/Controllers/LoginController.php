<?php

namespace App\Controllers;

use App\Models\UserModel;

class LoginController extends BaseController
{
    protected $userModel;
    public function __construct()
    {
        $this->userModel = new UserModel();
    }
    public function index()
    {
        echo view('login/login');
    }

    public function login()
    {
        $username = $this->request->getPost('username');
        $password = $this->request->getPost('password');

        $result = $this->userModel->proccessLogin($username, $password);
        if ($result) {
            return redirect()->to(base_url());
        } else {
            return redirect()->to(base_url('login'));
            // echo view('login/login');
        }
        // $data = [
        //     'username' => $username,
        //     'password' => $password,
        //     'isi' => 'kombinasi/v_list'
        // ];
        // echo view('/v_wrapper', $data);
    }

    public function logout()
    {
        session_destroy();
        return redirect()->to(base_url('login'));
    }
}