<?php

namespace App\Controllers;

use App\Models\KombinasiModel;
use App\Models\FarmasetisModel;
use JsonSerializable;

class KombinasiController extends BaseController
{

    protected $obj;
    protected $farmasetis;

    public function __construct()
    {
        $this->obj = new KombinasiModel();
        $this->farmasetis = new FarmasetisModel();
    }

    public function index()
    {
        $currentPage = $this->request->getVar('page') ? $this->request->getVar('page') : 1;

        $keyword = $this->request->getVar('keyword');
        if ($keyword) {
            $farmasetisModel = $this->farmasetis->search($keyword);
        } else {
            $farmasetisModel = $this->farmasetis->getFarmasetisList();
        }
        $data = [
            'title' => 'Daftar Kombinasi',
            'farmasetis' =>  $farmasetisModel->paginate(5),
            'pager' => $this->farmasetis->pager,
            'currentPage' => $currentPage,
            'isi' => 'kombinasi/v_list'
        ];

        echo view('layout/v_wrapper', $data);
    }

    public function view()
    {

        $search = $_POST['search']['value']; // Ambil data yang di ketik user pada textbox pencarian
        $limit = $_POST['length']; // Ambil data limit per page
        $start = $_POST['start']; // Ambil data start
        $order_index = $_POST['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
        $order_field = $_POST['columns'][$order_index]['data']; // Untuk mengambil nama field yg menjadi acuan untuk sorting
        $order_ascdesc = $_POST['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"
        $sql_total = $this->farmasetis->countAllResults(); // Panggil fungsi count_all pada SiswaModel
        $sql_data = $this->farmasetis->filter($search, $limit, $start, $order_field, $order_ascdesc); // Panggil fungsi filter pada SiswaModel
        $sql_filter = $this->farmasetis->count_filter($search); // Panggil fungsi count_filter pada SiswaModel
        $callback = array(
            'draw' => $_POST['draw'], // Ini dari datatablenya
            'recordsTotal' => $sql_total,
            'recordsFiltered' => $sql_filter,
            'data' => $sql_data
        );
        header('Content-Type: application/json');
        echo json_encode($callback); // Convert array $callback ke json
    }

    public function selectAll()
    {
        return json_encode($this->obj->selectAll());
    }

    public function openForm()
    {
        $data = [
            'title' => 'Kombinasi',
            'isi' => 'kombinasi/v_add'
        ];

        echo view('layout/v_wrapper', $data);
    }

    public function openFormEdit($id)
    {
        $data = [
            'title' => 'Daftar Kombinasi',
            'kombinasi' => $this->obj->findById($id),
            'isi' => 'kombinasi/v_edit'
        ];

        echo view('layout/v_wrapper', $data);
    }

    public function save()
    {
        $data = [
            "farmasetis_x" => $this->request->getVar('farmasetis_x'),
            "farmasetis_y" => $this->request->getVar('farmasetis_y'),
            "interaksi_obat" => $this->request->getVar('interaksi_obat')
        ];
        $this->obj->insertKombinasi($data);
        session()->setFlashdata('success', 'data berhasil ditambahkan');
        return redirect()->to(base_url('kombinasi_list'));
    }

    public function update($id)
    {
        $data = [
            "farmasetis_x" => $this->request->getVar('farmasetis_x'),
            "farmasetis_y" => $this->request->getVar('farmasetis_y'),
            "interaksi_obat" => $this->request->getVar('interaksi_obat')
        ];
        $this->obj->updateKombinasi($data, $id);
        session()->setFlashdata('success', 'data berhasil diupdate');
        return redirect()->to(base_url('kombinasi_list'));
    }

    public function delete($farmasetis_x, $farmasetis_y)
    {
        $response = [];
        $result = $this->obj->deleteKombinasi($farmasetis_x, $farmasetis_y);
        if ($result) {
            $response = [
                "code" => 200,
                "message" => "Success"
            ];
        } else {
            $response = [
                "code" => "00",
                "message" => "Gagal menghapus data Kombinasi"
            ];
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function detailKombinasi()
    {
        $id = $this->request->getVar('farmasetis_id');
        $data = [
            'farmasetis' => $this->farmasetis->findById($id),
            'kombinasi' => $this->obj->detailKombinasi($id)
        ];
        echo json_encode($data);
    }

    public function dragCompare()
    {
        $obat_a = $this->request->getVar('obat_a');
        $obat_b = $this->request->getVar('obat_b');
        echo json_encode($this->obj->dragCompare($obat_a, $obat_b));
    }

    public function insertMultiple()
    {
        if ($this->request->isAJAX()) {
            $data = $this->request->getJSON();
            return $this->obj->insertMultiple($data);
        }
        // session()->setFlashdata('success', 'data berhasil dihapus');
        // return redirect()->to(base_url('kombinasi_list'));
        // $id = $this->request->getBody('farmasetis_x');
        // $data = $this->request->getBody('farmasetis_y');
        // $this->obj->insertMultiple($id, $data);
    }
}