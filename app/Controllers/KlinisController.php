<?php

namespace App\Controllers;

use App\Models\KlinisModel;
use App\Models\ResepObatModel;

class KlinisController extends BaseController
{

    protected $obj;
    protected $resep;

    public function __construct()
    {
        $this->obj = new KlinisModel();
        $this->resep = new ResepObatModel();
    }

    public function index()
    {
        $currentPage = $this->request->getVar('page') ? $this->request->getVar('page') : 1;

        $keyword = $this->request->getVar('keyword');
        if ($keyword) {
            $farmasetisModel = $this->obj->search($keyword);
        } else {
            $farmasetisModel = $this->obj->getKlinisList();
        }
        $data = [
            'title' => 'Daftar Klinis',
            // 'klinis' =>  $farmasetisModel->paginate(5),
            // 'pager' => $this->obj->pager,
            // 'currentPage' => $currentPage,
            'isi' => 'klinis/v_list'
        ];

        echo view('layout/v_wrapper', $data);
    }

    public function view()
    {

        $search = $_POST['search']['value']; // Ambil data yang di ketik user pada textbox pencarian
        $limit = $_POST['length']; // Ambil data limit per page
        $start = $_POST['start']; // Ambil data start
        $order_index = $_POST['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
        $order_field = $_POST['columns'][$order_index]['data']; // Untuk mengambil nama field yg menjadi acuan untuk sorting
        $order_ascdesc = $_POST['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"
        $sql_total = $this->obj->countAllResults(); // Panggil fungsi count_all pada SiswaModel
        $sql_data = $this->obj->filter($search, $limit, $start, $order_field, $order_ascdesc); // Panggil fungsi filter pada SiswaModel
        $sql_filter = $this->obj->count_filter($search); // Panggil fungsi count_filter pada SiswaModel
        $callback = array(
            'draw' => $_POST['draw'], // Ini dari datatablenya
            'recordsTotal' => $sql_total,
            'recordsFiltered' => $sql_filter,
            'data' => $sql_data
        );
        header('Content-Type: application/json');
        echo json_encode($callback); // Convert array $callback ke json
    }

    public function openForm()
    {
        $data = [
            'title' => 'Klinis',
            'isi' => 'klinis/v_add'
        ];

        echo view('layout/v_wrapper', $data);
    }

    public function openFormEdit($id)
    {
        $data = [
            'klinis' => $this->obj->findById($id),
            'resep' => $this->resep->findByKlinisId($id)
        ];

        return json_encode($data);
    }

    public function save()
    {
        $data = [
            "indikasi_obat" => $this->request->getVar('indikasi_obat'),
            "dosis_obat" => $this->request->getVar('dosis_obat'),
            "rute_pemberian_obat" => $this->request->getVar('rute_pemberian_obat'),
            "tepat_waktu" => $this->request->getVar('tepat_waktu'),
            "duplikasi" => $this->request->getVar('duplikasi'),
            "alergi" => $this->request->getVar('alergi'),
            "interaksi_obat"  => $this->request->getVar('interaksi_obat'),
            "kontraindikasi_obat" => $this->request->getVar('kontraindikasi_obat'),
            "efek_samping" => $this->request->getVar('efek_samping')
        ];
        $this->obj->insertKlinis($data);
        session()->setFlashdata('success', 'data berhasil ditambahkan');
        return redirect()->to(base_url('klinis_list'));
    }

    public function update($id)
    {
        $data = [
            "administrasi_id" => $this->request->getVar('administrasi_id'),
        ];
        $this->obj->updateKlinis($data, $id);
        session()->setFlashdata('success', 'data berhasil diupdate');
        return redirect()->to(base_url('klinis_list'));
    }

    public function delete($id)
    {
        // $this->obj->deleteKlinis($id);
        $response = [];
        $result = $this->obj->deleteKlinis($id);
        if ($result) {
            $response = [
                "code" => 200,
                "message" => "Success"
            ];
        } else {
            $response = [
                "code" => "00",
                "message" => "Gagal menghapus data klinis"
            ];
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function insertKlinis()
    {
        if ($this->request->isAJAX()) {
            $data = $this->request->getJSON();
            // var_dump($data);
            $this->obj->insertKlinis($data);
        }
    }

    public function updateKlinis($id)
    {
        if ($this->request->isAJAX()) {
            $data = $this->request->getJSON();
            // var_dump($data);
            $this->obj->updateKlinis($data, $id);
        }
    }

    public function reportKlinis()
    {
        $data = [
            'title' => 'Laporan',
            'isi' => 'klinis/v_report'
        ];

        echo view('layout/v_wrapper', $data);
    }

    public function viewReport()
    {

        $search = $_POST['search']['value']; // Ambil data yang di ketik user pada textbox pencarian
        $limit = $_POST['length']; // Ambil data limit per page
        $start = $_POST['start']; // Ambil data start
        $order_index = $_POST['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
        $order_field = $_POST['columns'][$order_index]['data']; // Untuk mengambil nama field yg menjadi acuan untuk sorting
        $order_ascdesc = $_POST['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"
        $sql_total = $this->obj->countAllResults(); // Panggil fungsi count_all pada SiswaModel
        $sql_data = $this->obj->filterReport($search, $limit, $start, $order_field, $order_ascdesc); // Panggil fungsi filter pada SiswaModel
        $sql_filter = $this->obj->count_filterReport($search); // Panggil fungsi count_filter pada SiswaModel
        $callback = array(
            'draw' => $_POST['draw'], // Ini dari datatablenya
            'recordsTotal' => $sql_total,
            'recordsFiltered' => $sql_filter,
            'data' => $sql_data
        );
        header('Content-Type: application/json');
        echo json_encode($callback); // Convert array $callback ke json
    }

    public function reportDetail($id)
    {
        $data = [
            'klinis' => $this->obj->findById($id),
            'resep' => $this->resep->findByKlinisId($id)
        ];

        return json_encode($data);
    }
}