<?php

namespace App\Controllers;

use App\Models\AboutModel;

class AboutController extends BaseController
{
    protected $aboutModel;
    public function __construct()
    {
        $this->aboutModel = new AboutModel();
    }

    public function index()
    {
        $data = [
            "title" => "About",
            "isi" => "v_about",
        ];

        echo view('layout/v_wrapper', $data);
    }

    public function insertAbout()
    {
        $photo = $this->request->getFile('photo_instansi');
        $newName = $photo->getRandomName();

        // $photo->move(ROOTPATH . 'public/uploads', $newName);
        $photo->move(ROOTPATH . 'uploads', $newName);
        $deskripsi = $this->request->getPost('deskripsi');
        $data = [
            'photo' => $newName,
            'description' => $deskripsi . " file name in path" . WRITEPATH . " === root path " . ROOTPATH,
        ];
        $this->aboutModel->insetAbout($data);
        return redirect()->to(base_url());
    }
}