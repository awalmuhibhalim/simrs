<?php

namespace App\Controllers;

use App\Models\FarmasetisModel;
use App\Models\KombinasiModel;

class FarmasetisController extends BaseController
{

    protected $obj;
    protected $kombinasi;

    public function __construct()
    {
        $this->obj = new FarmasetisModel();
        $this->kombinasi = new KombinasiModel();
    }

    public function index()
    {
        $currentPage = $this->request->getVar('page') ? $this->request->getVar('page') : 1;

        $keyword = $this->request->getVar('keyword');
        if ($keyword) {
            $farmasetisModel = $this->obj->search($keyword);
        } else {
            $farmasetisModel = $this->obj->getFarmasetisList();
        }
        $data = [
            'title' => 'Daftar Farmasetis',
            'farmasetis' =>  $farmasetisModel->paginate(5),
            'pager' => $this->obj->pager,
            'currentPage' => $currentPage,
            'isi' => 'farmasetis/v_list'
        ];

        echo view('layout/v_wrapper', $data);
    }

    public function view()
    {

        $search = $_POST['search']['value']; // Ambil data yang di ketik user pada textbox pencarian
        $limit = $_POST['length']; // Ambil data limit per page
        $start = $_POST['start']; // Ambil data start
        $order_index = $_POST['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
        $order_field = $_POST['columns'][$order_index]['data']; // Untuk mengambil nama field yg menjadi acuan untuk sorting
        $order_ascdesc = $_POST['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"
        $sql_total = $this->obj->countAllResults(); // Panggil fungsi count_all pada SiswaModel
        $sql_data = $this->obj->filter($search, $limit, $start, $order_field, $order_ascdesc); // Panggil fungsi filter pada SiswaModel
        $sql_filter = $this->obj->count_filter($search); // Panggil fungsi count_filter pada SiswaModel
        $callback = array(
            'draw' => $_POST['draw'], // Ini dari datatablenya
            'recordsTotal' => $sql_total,
            'recordsFiltered' => $sql_filter,
            'data' => $sql_data
        );
        header('Content-Type: application/json');
        echo json_encode($callback); // Convert array $callback ke json
    }

    public function openForm()
    {
        $data = [
            'title' => 'Faramsetis',
            'isi' => 'farmasetis/v_add'
        ];

        echo view('layout/v_wrapper', $data);
    }

    public function openFormEdit($id)
    {
        return json_encode($this->obj->findById($id));
    }

    public function save()
    {
        $nama_obat  = $_POST['nama_obat'];
        $bentuk_sediaan  = $_POST['bentuk_sediaan'];
        $kekuatan_sediaan  = $_POST['kekuatan_sediaan'];
        $satuan_sediaan  = $_POST['satuan_sediaan'];
        $jumlah_obat = $_POST['jumlah_obat'];
        $aturan_pakai  = $_POST['aturan_pakai'];
        $cara_pakai  = $_POST['cara_pakai'];
        $stabilitas_obat = $_POST['stabilitas_obat'];
        $indikasi_obat = $_POST['indikasi_obat'];
        $dosis_obat = $_POST['dosis_obat'];
        $rute_pemberian_obat = $_POST['rute_pemberian_obat'];
        $tepat_waktu = $_POST['tepat_waktu'];
        $duplikasi = $_POST['duplikasi'];
        $alergi = $_POST['alergi'];
        $interaksi_obat = $_POST['interaksi_obat'];
        $kontraindikasi_obat = $_POST['kontraindikasi_obat'];
        $efek_samping = $_POST['efek_samping'];

        $data = [
            "nama_obat" => $nama_obat,
            "bentuk_sediaan" => $bentuk_sediaan,
            "kekuatan_sediaan" => $kekuatan_sediaan,
            "satuan_sediaan" => $satuan_sediaan,
            "jumlah_obat" => $jumlah_obat,
            "aturan_pakai" => $aturan_pakai,
            "cara_pakai" => $cara_pakai,
            "stabilitas_obat" => $stabilitas_obat,
            "indikasi_obat" => $indikasi_obat,
            "dosis_obat" => $dosis_obat,
            "rute_pemberian_obat" => $rute_pemberian_obat,
            "tepat_waktu" => $tepat_waktu,
            "duplikasi" => $duplikasi,
            "alergi" => $alergi,
            "interaksi_obat"  => $interaksi_obat,
            "kontraindikasi_obat" => $kontraindikasi_obat,
            "efek_samping" => $efek_samping,
            "created_at" => date('d-M-Y h:t:s'),
            "creator" => $_SESSION['username']
        ];
        $this->obj->insertFarmasetis($data);
    }

    public function update($id)
    {
        $nama_obat  = $_POST['nama_obat'];
        $bentuk_sediaan  = $_POST['bentuk_sediaan'];
        $kekuatan_sediaan  = $_POST['kekuatan_sediaan'];
        $satuan_sediaan  = $_POST['satuan_sediaan'];
        $jumlah_obat = $_POST['jumlah_obat'];
        $aturan_pakai  = $_POST['aturan_pakai'];
        $cara_pakai  = $_POST['cara_pakai'];
        $stabilitas_obat = $_POST['stabilitas_obat'];
        $indikasi_obat = $_POST['indikasi_obat'];
        $dosis_obat = $_POST['dosis_obat'];
        $rute_pemberian_obat = $_POST['rute_pemberian_obat'];
        $tepat_waktu = $_POST['tepat_waktu'];
        $duplikasi = $_POST['duplikasi'];
        $alergi = $_POST['alergi'];
        $interaksi_obat = $_POST['interaksi_obat'];
        $kontraindikasi_obat = $_POST['kontraindikasi_obat'];
        $efek_samping = $_POST['efek_samping'];

        $data = [
            "nama_obat" => $nama_obat,
            "bentuk_sediaan" => $bentuk_sediaan,
            "kekuatan_sediaan" => $kekuatan_sediaan,
            "satuan_sediaan" => $satuan_sediaan,
            "jumlah_obat" => $jumlah_obat,
            "aturan_pakai" => $aturan_pakai,
            "cara_pakai" => $cara_pakai,
            "stabilitas_obat" => $stabilitas_obat,
            "indikasi_obat" => $indikasi_obat,
            "dosis_obat" => $dosis_obat,
            "rute_pemberian_obat" => $rute_pemberian_obat,
            "tepat_waktu" => $tepat_waktu,
            "duplikasi" => $duplikasi,
            "alergi" => $alergi,
            "interaksi_obat"  => $interaksi_obat,
            "kontraindikasi_obat" => $kontraindikasi_obat,
            "efek_samping" => $efek_samping,
            "updated_at" => date('d-M-Y h:t:s'),
            "creator" => $_SESSION['username']
        ];
        $this->obj->updateFarmasetis($data, $id);
        return "sukses";
    }

    // public function update($id)
    // {
    //     var_dump($_POST['kekuatan_sediaan']);
    //     if ($this->request->isAJAX()) {
    //         $temp = $this->request->getJSON();
    //         $temp2 = $this->request->getRawInput();
    //         $nama_obat  = str_replace('"', '', json_encode($temp->nama_obat));
    //         $bentuk_sediaan  = str_replace('"', '', json_encode($temp->bentuk_sediaan));
    //         $kekuatan_sediaan  = str_replace('"', '', json_encode($temp->kekuatan_sediaan));
    //         $satuan_sediaan  = str_replace('"', '', json_encode($temp->satuan_sediaan));
    //         $jumlah_obat = str_replace('"', '', json_encode($temp->jumlah_obat));
    //         $aturan_pakai  = str_replace('"', '', json_encode($temp->aturan_pakai));
    //         $cara_pakai  = str_replace('"', '', json_encode($temp->cara_pakai));
    //         $stabilitas_obat = str_replace('"', '', json_encode($temp->stabilitas_obat));
    //         $indikasi_obat = str_replace('"', '', json_encode($temp->indikasi_obat));
    //         $dosis_obat = str_replace('"', '', json_encode($temp->dosis_obat));
    //         $rute_pemberian_obat = str_replace('"', '', json_encode($temp->rute_pemberian_obat));
    //         $tepat_waktu = str_replace('"', '', json_encode($temp->tepat_waktu));
    //         $duplikasi = str_replace('"', '', json_encode($temp->duplikasi));
    //         $alergi = str_replace('"', '', json_encode($temp->alergi));
    //         $interaksi_obat = str_replace('"', '', json_encode($temp->interaksi_obat));
    //         $kontraindikasi_obat = str_replace('"', '', json_encode($temp->kontraindikasi_obat));
    //         $efek_samping = str_replace('"', '', json_encode($temp->efek_samping));

    //         $data = [
    //             "nama_obat" => $nama_obat,
    //             "bentuk_sediaan" => $bentuk_sediaan,
    //             "kekuatan_sediaan" => $kekuatan_sediaan,
    //             "satuan_sediaan" => $satuan_sediaan,
    //             "jumlah_obat" => $jumlah_obat,
    //             "aturan_pakai" => $aturan_pakai,
    //             "cara_pakai" => $cara_pakai,
    //             "stabilitas_obat" => $stabilitas_obat,
    //             "indikasi_obat" => $indikasi_obat,
    //             "dosis_obat" => $dosis_obat,
    //             "rute_pemberian_obat" => $rute_pemberian_obat,
    //             "tepat_waktu" => $tepat_waktu,
    //             "duplikasi" => $duplikasi,
    //             "alergi" => $alergi,
    //             "interaksi_obat"  => $interaksi_obat,
    //             "kontraindikasi_obat" => $kontraindikasi_obat,
    //             "efek_samping" => $efek_samping,
    //             "updated_at" => date('d-M-Y h:t:s'),
    //             "creator" => $_SESSION['username']
    //         ];
    //         $this->obj->updateFarmasetis($data, $id);
    //     }
    // }

    public function delete($id)
    {
        $response = [];
        $result = $this->obj->deleteFarmasetis($id);
        if ($result) {
            $response = [
                "code" => 200,
                "message" => "Success"
            ];
        } else {
            $response = [
                "code" => "00",
                "message" => "Data Farmasetis sedang digunakan di kombinasi obat"
            ];
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function findById($id)
    {
        return $this->obj->findById($id);
    }

    public function findAllFarmasetis()
    {
        echo json_encode($this->obj->retrieveAllFarmasetis());
    }

    public function getAllFarmasetisEx()
    {
        $id = $this->request->getVar('id');
        $data = [
            'farmasetis' =>  $this->obj->getAllFarmasetisEx($id),
            'kombinasi_obat' => $this->kombinasi->detailKombinasi($id)
        ];
        echo json_encode($data);
    }

    public function serachFarmasetis()
    {
        $keyword = $this->request->getVar('keyword');
        echo json_encode($this->obj->searchFarmasetis($keyword));
    }

    public function getFarmasetisAutoComplete()
    {
        $postData = $this->request->getPost('search');
        $data = $this->obj->getFarmasetisAutoComplete($postData);

        echo json_encode($data);
    }
}