<?php

namespace App\Controllers;

use App\Models\HistoryModel;

class HistoryController extends BaseController
{
    protected $history;

    public function __construct()
    {
        $this->history = new HistoryModel();
    }

    public function index()
    {

        $data = [
            'title' => 'Daftar History',
            // 'users' => $userModel->paginate(5),
            // 'pager' => $this->UserModel->pager,
            // 'currentPage' => $currentPage,
            // 'users' => $this->UserModel->getUserList(),
            'isi' => 'history/v_list'
        ];

        echo view('layout/v_wrapper', $data);
    }


    public function view()
    {

        $search = $_POST['search']['value']; // Ambil data yang di ketik user pada textbox pencarian
        $limit = $_POST['length']; // Ambil data limit per page
        $start = $_POST['start']; // Ambil data start
        $order_index = $_POST['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
        $order_field = $_POST['columns'][$order_index]['data']; // Untuk mengambil nama field yg menjadi acuan untuk sorting
        $order_ascdesc = $_POST['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"
        $sql_total = $this->history->countAllResults(); // Panggil fungsi count_all pada SiswaModel
        $sql_data = $this->history->filter($search, $limit, $start, $order_field, $order_ascdesc); // Panggil fungsi filter pada SiswaModel
        $sql_filter = $this->history->count_filter($search); // Panggil fungsi count_filter pada SiswaModel
        $callback = array(
            'draw' => $_POST['draw'], // Ini dari datatablenya
            'recordsTotal' => $sql_total,
            'recordsFiltered' => $sql_filter,
            'data' => $sql_data
        );
        header('Content-Type: application/json');
        echo json_encode($callback); // Convert array $callback ke json
    }
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */