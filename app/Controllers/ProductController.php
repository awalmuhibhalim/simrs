<?php

namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\ProductModel;

class ProductController extends BaseController {

    protected $ProductModel;

    public function __construct() {
        $this->ProductModel = new ProductModel();
    }

    public function index() {
        try {
            $data = [
                'title' => 'List Product',
                'product' => $this->ProductModel->getAllProduct(),
                'isi' => 'product/v_list'
            ];
            echo view('layout/v_wrapper', $data);
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    //--------------------------------------------------------------------
}
