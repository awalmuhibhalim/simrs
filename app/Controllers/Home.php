<?php namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		$data=[
			'title' => '',
			'isi' => 'v_home'
		];
		echo view('layout/v_wrapper', $data);
	}
	
	public function halaman2()
	{
		$data = [
			'title' => 'Halaman 2',
			'isi' => 'v_halaman_2'
		];
		echo view('layout/v_wrapper', $data);
	}

	//--------------------------------------------------------------------

}