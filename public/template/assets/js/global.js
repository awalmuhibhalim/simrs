$(document).ready(function () {
  // Initialize
  $("#serach_global").autocomplete({
    source: function (request, response) {
      // Fetch data
      $.ajax({
        url: "FarmasetisController/getFarmasetisAutoComplete",
        type: "post",
        dataType: "json",
        data: {
          search: request.term,
        },
        success: function (data) {
          response(data);
        },
      });
    },
    select: function (event, ui) {
      tampilkanObat(ui.item.value);
      // Set selection
      // $("#autouser").val(ui.item.label); // display the selected text
      // $("#userid").val(ui.item.value); // save selected id to input
      return false;
    },
  });
});

function tampilkanObat(id) {
  $.ajax({
    method: "GET",
    url: "edit_farmasetis/" + id,
    contentType: "application/json",
  }).done(function (response) {
    let data = JSON.parse(response);
    console.log(data);
    $("#info_detail_obat").empty();
    let tr =
      "<tr>\n\
      <td align='center'><h4>Farmasetis</h4></td>\n\
      <td></td>\n\
    </tr>\n\
    <tr>\n\
                <td>Nama Obat</td>\n\
                <td>" +
      data.nama_obat +
      "</td>\n\
              </tr>\n\
              <tr>\n\
                <td>Bentuk Sediaan</td>\n\
                <td>" +
      data.bentuk_sediaan +
      "</td>\n\
              </tr>\n\
              <tr>\n\
                <td>Kekuatan Sediaan</td>\n\
                <td>" +
      data.kekuatan_sediaan +
      "</td>\n\
              </tr>\n\
              <tr>\n\
                <td>Satuan Sediaan</td>\n\
                <td>" +
      data.satuan_sediaan +
      "</td>\n\
              </tr>\n\
              <tr>\n\
                <td>Jumlah Obat</td>\n\
                <td>" +
      data.jumlah_obat +
      "</td>\n\
              </tr>\n\
              <tr>\n\
                <td>Aturan Pakai</td>\n\
                <td>" +
      data.aturan_pakai +
      "</td>\n\
              </tr>\n\
              <tr>\n\
                <td>Cara Pakai</td>\n\
                <td>" +
      data.cara_pakai +
      "</td>\n\
              </tr>\n\
              <tr>\n\
                <td>Stabilitas Obat</td>\n\
                <td>" +
      data.stabilitas_obat +
      "</td>\n\
              </tr>\n\
              <tr>\n\
                <td align='center'><h4>Kinis</h4></td>\n\
                <td></td>\n\
              </tr>\n\
              <tr>\n\
                <td>Indikasi Obat</td>\n\
                <td>" +
      data.indikasi_obat +
      "</td>\n\
              </tr>\n\
              <tr>\n\
                <td>Dosis Obat</td>\n\
                <td>" +
      data.dosis_obat +
      "</td>\n\
              </tr>\n\
              <tr>\n\
                <td>Rute Pemberian Obat</td>\n\
                <td>" +
      data.rute_pemberian_obat +
      "</td>\n\
              </tr>\n\
              <tr>\n\
                <td>Tepat Waktu</td>\n\
                <td>" +
      data.tepat_waktu +
      "</td>\n\
              </tr>\n\
              <tr>\n\
                <td>Duplikasi</td>\n\
                <td>" +
      data.duplikasi +
      "</td>\n\
              </tr>\n\
              <tr>\n\
                <td>Alergi</td>\n\
                <td>" +
      data.alergi +
      "</td>\n\
              </tr>\n\
              <tr>\n\
                <td>Interaksi Obat</td>\n\
                <td>" +
      data.interaksi_obat +
      "</td>\n\
              </tr>\n\
              <tr>\n\
                <td>Kontraindikasi Obat</td>\n\
                <td>" +
      data.kontraindikasi_obat +
      "</td>\n\
              </tr>\n\
              <tr>\n\
                <td>Efek Samping</td>\n\
                <td>" +
      data.efek_samping +
      "</td>\n\
              </tr>";
    $("#info_detail_obat").append(tr);
    $("#modal-info-obat").modal("show");
  });
}
